/*
The Enigma Protector v2.x - v3.x 32bit Devirtualizer
This tool is designed to translate the virtual opcodes of the Enigma
VM back to normal x86 Code.

Author: DizzY_D & Raham
Version: 1.4

Credits:
Ap0x for TitanEngine
herumi for XBYAK
f0Gx, n0p0x90, BlackBerry for all their great support

Usage:
Compile this source with MSVC++2010 to a native 32bit DLL.
Load the DLL into the target's process space.
For the first (Enigma internal) VM use the "InnerDevirtualize" function
exported by this DLL.
For the second (SDK) VM use the "OuterDevirtualize" function exported by
this DLL and pass a pointer to the bytecode as a parameter.
The VM code should now automatically be fixed and run completely
independent from the Enigma VM.

Notes:
This was one of my first c++ projects so please don't expect super
perfect code. I also didn't implement exception handling and such, but
now it's open source, so why not implementing it yourself?
It would be nice to see if someone would take up this project and prepare
it for future versions of Enigma.


Copyright (C) 2012 DizzY_D
Improved By Raham.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include <map>
#include <windows.h>
#include "global.h"
#include "VM.h"

static ULONG_PTR pOuterVA=0;
static ULONG_PTR pOuterSize=0;

BOOL APIENTRY DllMain(HINSTANCE hInst, ULONG_PTR reason, LPVOID reserved)
{
    return true;
}

struct DevirtualizeResult
{
    ULONG_PTR DevirtualizedDest;
    ULONG_PTR Size;
    ULONG_PTR EntriesFixed;
};

static ULONG_PTR OuterDevirtualizeInternal(HMODULE hModule, ULONG_PTR intDest)
{
    VM* outerVM = new VM();
    static DevirtualizeResult Result;
    ULONG_PTR iRes=outerVM->DevirtualizeExternal((ULONG_PTR)hModule, intDest);
    if(iRes)
    {
        pOuterVA=outerVM->pDecompiledCode;
        pOuterSize=outerVM->pSize+0x100;
        Result.DevirtualizedDest= outerVM->pDecompiledCode;
        Result.Size=outerVM->pSize;
        Result.EntriesFixed=iRes;
        delete outerVM;
        return ULONG_PTR(&Result);
    }
    delete outerVM;
    return 0;
}

/*  Function:
        OuterDevirtualize (STDCALL)
    Description:
        Devirtualizes Enigma VM code inside a module.
    Parameters:
        hModule: Base of the module to devirtualize.
        intDest:
                    0 means 'devirtualize on spot'
                    1 means 'devirtualize in a newly allocated memory block'
                    ? means 'devirtualize at ADDR pointed to by ?'
    Returns:
        0 on failure, a pointer to the devirtualized block otherwise.
*/
__declspec(dllexport) ULONG_PTR __stdcall OuterDevirtualize(HMODULE hModule, ULONG_PTR intDest)
{
    ULONG_PTR retVal=0;
    __try
    {
        retVal=OuterDevirtualizeInternal(hModule, intDest);
    }
    __except(EXCEPTION_EXECUTE_HANDLER )
    {
        MessageBoxA(0, "Exception in 'OuterDevirtualize'", "Exception!", MB_SYSTEMMODAL|MB_ICONERROR);
    }
    return retVal;
}

static ULONG_PTR InnerDevirtualizeInternal(HMODULE hModule)
{
    VM* innerVM = new VM();
    static DevirtualizeResult Result;
    ULONG_PTR iRes=innerVM->DevirtualizeInternal((ULONG_PTR)hModule);
    if(iRes)
    {
        pOuterVA=innerVM->pDecompiledCode;
        pOuterSize=innerVM->pSize+0x100;
        Result.DevirtualizedDest= innerVM->pDecompiledCode;
        Result.Size=innerVM->pSize;
        Result.EntriesFixed=iRes;
        delete innerVM;
        return ULONG_PTR(&Result);
    }
    delete innerVM;
    return 0;
}

/*  Function:
        InnerDevirtualize (STDCALL)
    Description:
        Devirtualizes the internal Enigma VM code.
    Parameters:
        hModule: Base of the module to devirtualize.
    Returns:
        0 on failure, a pointer to the devirtualized block otherwise.
*/
__declspec(dllexport) ULONG_PTR __stdcall InnerDevirtualize(HMODULE hModule)
{
    ULONG_PTR retVal=0;
    __try
    {
        retVal=InnerDevirtualizeInternal(hModule);
    }
    __except(EXCEPTION_EXECUTE_HANDLER )
    {
        MessageBoxA(0, "Exception in 'InnerDevirtualize'", "Exception!", MB_SYSTEMMODAL|MB_ICONERROR);
    }
    return retVal;
}

/*  Function:
        SaveOuterCode (STDCALL)
    Description:
        Saves the previously devirtualized code to a file.
    Parameters:
        szFileName: Pointer to filename to save the previously devirtualized code to.
    Returns:
        Nothing.
*/
__declspec(dllexport) void __stdcall SaveOuterCode(LPSTR szFileName)
{
    if(pOuterVA && pOuterSize)
    {
        HANDLE hFile=CreateFileA(szFileName, GENERIC_ALL, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
        if(hFile==INVALID_HANDLE_VALUE) //failed to create file
            return;
        __try
        {
            ULONG_PTR written=0;
            WriteFile(hFile, (LPCVOID)pOuterVA, pOuterSize, &written, 0);
        }
        __except(EXCEPTION_EXECUTE_HANDLER)
        {
        }        
        CloseHandle(hFile);
    }
}
