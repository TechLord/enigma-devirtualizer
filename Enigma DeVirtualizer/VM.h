/*
The Enigma Protector v2.x - v3.x 32bit Devirtualizer
This tool is designed to translate the virtual opcodes of the Enigma
VM back to normal x86 Code.

Author: DizzY_D & Raham
Version: 1.4

Credits:
Ap0x for TitanEngine
herumi for XBYAK
f0Gx, n0p0x90, BlackBerry for all their great support

Usage:
Load the DLL into the target's process space.
For the first (Enigma internal) VM use the "InnerDevirtualize" function
exported by this DLL.
For the second (SDK) VM use the "OuterDevirtualize" function exported by
this DLL and pass a pointer to the bytecode as a parameter.
The VM code should now automatically be fixed and run completely
independent from the Enigma VM.

Notes:
This was one of my first c++ projects so please don't expect super
perfect code. I also didn't implement exception handling and such, but
now it's open source, so why not implementing it yourself?
It would be nice to see if someone would take up this project and prepare
it for future versions of Enigma.



Copyright (C) 2012 DizzY_D
Improved By Raham.


This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, see <http://www.gnu.org/licenses/>.
*/


#pragma once

#include "global.h"
#include "TitanEngine\TitanEngine.h"
#include <map>
#include "VCodehandler.h"
#include "Relocater.h"
#include "EnigmaAnalyzer.h"
class VM
{
public:
    VM();                                                                // Standard Constructor
    ULONG_PTR DevirtualizeInternal(ULONG_PTR);                                    // Devirtualize Enigma VDLL
    ULONG_PTR DevirtualizeExternal(ULONG_PTR,ULONG_PTR);                            // Devirtualize Target Code
    //bool DevirtualizeByAddress();
    ULONG_PTR pDecompiledCode;
    ULONG_PTR pSize;
    static ULONG_PTR AllocateMem(ULONG_PTR,ULONG_PTR);
    ULONG_PTR VM_Version;
    LPVOID FindPCodeTable(LPVOID);
protected:
private:
    ULONG_PTR pushXOR;
    ULONG_PTR pBYTECODE;
    int DetectVMVersion(ULONG_PTR vMAddr);                                    // Detect Enigma Version by number of VM Handler
    const ULONG_PTR GetVMAddrFromDispatcher(ULONG_PTR dispatcher);                    // Get Handler Control Flow Function by VM Start Address
    lModInfo GetSectionBase(ULONG_PTR,int);                                // Get Section Address from Section Number
    BOOL FindVM(lModInfo modInfo);                                        // Find internal and external VM and fill dispatcher array
    ULONG_PTR FindDispatcher(ULONG_PTR);                                        // Get VM dispatcher address
    //ULONG_PTR FindByteCode(lModInfo modInfo, int numberOfHandler);            // Get virtual bytecode start address
    ULONG_PTR VM::FindByteCode(ULONG_PTR dispatcher);
    void LogVParts(std::map<ULONG_PTR,refs> &refMap, ULONG_PTR, lModInfo modInfo, int,int CodeCount=0);        // Log all VM entry PUSH/JMPs and fill key+address for relocater
    ULONG_PTR CalcByteCodeVAfromPushVal(ULONG_PTR,ULONG_PTR);                        // Calculate byte code address from key value
    void ConvertByteCode(std::map<ULONG_PTR, refs> &entryList, std::map<ULONG_PTR,ULONG_PTR> &RelocMap, std::map<ULONG_PTR, ULONG_PTR> &RelocMapAddr,std::map<int, ULONG_PTR> &KeyMap, lModInfo mInf);        // Byte code converter routine
    void ConvertByteCodeNew(std::map<ULONG_PTR, refs> &entryList, std::map<ULONG_PTR,ULONG_PTR> &RelocMap, std::map<ULONG_PTR, ULONG_PTR> &RelocMapAddr,std::map<int, ULONG_PTR> &KeyMap, lModInfo mInf,bool IndexOrRVA=true);        // Byte code converter routine
    lModInfo modInfo;
    ULONG_PTR dispatcher[2];
    void FindXORValue(ULONG_PTR dispatcher);
    ULONG_PTR pIndexTable;

    std::vector<VirtualCodeHandler*>* handler;
    std::map<ULONG_PTR,ULONG_PTR>* RelocMapKey;
    std::map<ULONG_PTR,ULONG_PTR>* RelocMapAddr;
    std::map<int, ULONG_PTR>* KeyMap;
    std::map<ULONG_PTR,refs>* entries;
};

