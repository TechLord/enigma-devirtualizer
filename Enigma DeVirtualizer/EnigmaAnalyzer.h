#pragma once
#include <Windows.h>
#include "PatternDetection.h"
#include "BeaEngine\BeaEngine.h"
#include <stack>
#include <list>
#include <map>
#include <vector>
#include "TitanEngine\TitanEngine.h"

class CEnigmaAnalyzer
{
public:
    enum HandlerType {Undifined_Handler,
                      SUB_1_None_Imm32,SUB_1_Single_Reg,SUB_1_Single_Imm32,SUB_1_Double_REG,SUB_1_None_Imm8,SUB_1_Single_Mem32_Reg_Imm,SUB_1_Single_Mem32_Reg,SUB_1_Double_Mem32_Reg_Imm,SUB_1_Double_Mem32_Reg,
                      SUB_1_X_16,SUB_1_X_32,SUB_1_X_8,SUB_1_X_8High,SUB_1_Single_Imm8,SUB_1_Double_Mem16_Reg_Imm,SUB_1_Double_Mem8_Reg_Imm8,SUB_1_Double_Mem32_Reg_Imm8,SUB_1_Double_Mem8_Reg_Imm32,SUB_1_Single_Mem8_Reg,
                      SUB_1_Single_Mem32_Reg_Imm8,SUB_1_Single_Mem8_Reg_Imm8,SUB_1_Single_Mem8_Reg_Imm32,SUB_1_Single_Mem16_Reg_Imm32,SUB_1_Single_Reg_Imm16,SUB_1_Double_Mem8_Reg,SUB_1_Single_Mem16_Reg,SUB_1_Double_Mem16_Reg_Imm8,SUB_1_Single_Mem16_Reg_Imm8,

                      SUB_2_MOV32,SUB_2_MOV16,SUB_2_MOV8,
                      SUB_2_AND32,SUB_2_AND16,SUB_2_AND8,
                      SUB_2_ADD32,SUB_2_ADD16,SUB_2_ADD8,
                      SUB_2_SUB32,SUB_2_SUB16,SUB_2_SUB8,
                      SUB_2_SBB32,SUB_2_SBB16,SUB_2_SBB8,
                      SUB_2_XOR32,SUB_2_XOR16,SUB_2_XOR8,
                      SUB_2_OR32,SUB_2_OR16,SUB_2_OR8,
                      SUB_2_ADC32,SUB_2_ADC16,SUB_2_ADC8,


                      SUB_3_Single,SUB_3_NULL,


                      SUB_3_1_32,SUB_3_1_16,SUB_3_1_8,SUB_3_1_8High
                     };

    enum OpcodeType {NONE,MOV,ADD,ADC,SUB,CMP,AND,TEST,XOR,OR,SBB};

    struct HandlerRecord
    {
        CPatternDetection::PatternType HandlerPattern[48];
        ULONG_PTR PatternCount;
        HandlerType HandlerID;
    };

    struct ASMOperand
    {
        ULONG_PTR Value;
        ULONG_PTR ExtraValue;
        bool RegOrImm;
        bool bHighReg;
        bool bMemory;
        unsigned char OperandSize;

    };

    HandlerRecord *PatternTable;
    ULONG_PTR HandlerCount;

    CEnigmaAnalyzer(void);
    ~CEnigmaAnalyzer(void);

    struct InstRecord
    {
        DISASM iDisasm;
        CPatternDetection::PatternType iType;
    };
    struct CacheRecord
    {
        std::vector<InstRecord> *HandlerInstruction;
        HandlerType iHandlerID;
    };
    std::vector<InstRecord> *HandlerInstruction;
    std::map<ULONG_PTR,CacheRecord*> HandlerTable;
    ULONG_PTR EnigmaBase;
    ULONG_PTR EnigmaSize;

    void GetEnigmaInfo(ULONG_PTR iMod);
    HandlerType GetHandlerType(ULONG_PTR VAddr);
    void AnalyzeOpcode(ULONG_PTR *Stream);


    ASMOperand Operand1;
    ASMOperand Operand2;
    OpcodeType CurrentOpcode;
    ULONG_PTR dwImageBase;
private:
    PDISASM FindInstruction(ULONG_PTR nStart,CPatternDetection::PatternType iType);
    ASMOperand AnalyzeOperand(HandlerType iOperandHandler=Undifined_Handler);
    HandlerType AnalyzeHandler();
    ULONG_PTR TraceCode(ULONG_PTR VAddr);
    enum BEARegister {EAX=1,ECX=2,EDX=4,EBX=8,ESP=16,EBP=32,ESI=64,EDI=128,NoneRegister=256,NotRegister=0};


    ULONG_PTR Sub_1;
    ULONG_PTR Sub_2;
    ULONG_PTR Sub_3;
    ULONG_PTR Sub_1_1;
    ULONG_PTR Sub_1_2;
    ULONG_PTR Sub_3_1;
    HandlerType Sub_1Type;
    HandlerType Sub_2Type;
    HandlerType Sub_3Type;
    HandlerType Sub_3_1_Type;
    HandlerType Sub_1_1_Type;
    HandlerType Sub_1_2_Type;
    ULONG_PTR GetRegIndex(ULONG_PTR iReg);

};

