/*
The Enigma Protector v2.x - v3.x 32bit Devirtualizer
This tool is designed to translate the virtual opcodes of the Enigma
VM back to normal x86 Code.

Author: DizzY_D & Raham
Version: 1.4

Credits:
Ap0x for TitanEngine
herumi for XBYAK
f0Gx, n0p0x90, BlackBerry for all their great support

Usage:
Load the DLL into the target's process space.
For the first (Enigma internal) VM use the "InnerDevirtualize" function
exported by this DLL.
For the second (SDK) VM use the "OuterDevirtualize" function exported by
this DLL and pass a pointer to the bytecode as a parameter.
The VM code should now automatically be fixed and run completely
independent from the Enigma VM.

Notes:
This was one of my first c++ projects so please don't expect super
perfect code. I also didn't implement exception handling and such, but
now it's open source, so why not implementing it yourself?
It would be nice to see if someone would take up this project and prepare
it for future versions of Enigma.



Copyright (C) 2012 DizzY_D
Improved By Raham.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, see <http://www.gnu.org/licenses/>.
*/


#include "VM.h"

VM::VM()
{
    //clear objects
    handler = new std::vector<VirtualCodeHandler*>();
    RelocMapKey = new std::map<ULONG_PTR,ULONG_PTR>();
    RelocMapAddr = new std::map<ULONG_PTR,ULONG_PTR>();
    KeyMap = new std::map<int, ULONG_PTR>();
    entries = new std::map<ULONG_PTR,refs>();

    handler->push_back((VirtualCodeHandler *)new Nop());
    handler->push_back((VirtualCodeHandler *)new Idiv());
    handler->push_back((VirtualCodeHandler *)new Div());
    handler->push_back((VirtualCodeHandler *)new Imul());
    handler->push_back((VirtualCodeHandler *)new Neg());
    handler->push_back((VirtualCodeHandler *)new Not());
    handler->push_back((VirtualCodeHandler *)new Movs());
    handler->push_back((VirtualCodeHandler *)new Stos());
    handler->push_back((VirtualCodeHandler *)new Lods());
    handler->push_back((VirtualCodeHandler *)new Cmps());
    handler->push_back((VirtualCodeHandler *)new Scas());
    handler->push_back((VirtualCodeHandler *)new Stc());
    handler->push_back((VirtualCodeHandler *)new Clc());
    handler->push_back((VirtualCodeHandler *)new Std());
    handler->push_back((VirtualCodeHandler *)new Cld());
    handler->push_back((VirtualCodeHandler *)new Cdq());
    handler->push_back((VirtualCodeHandler *)new Cmc());
    handler->push_back((VirtualCodeHandler *)new Rcr());
    handler->push_back((VirtualCodeHandler *)new Rcl());
    handler->push_back((VirtualCodeHandler *)new Shl());
    handler->push_back((VirtualCodeHandler *)new Sar());
    handler->push_back((VirtualCodeHandler *)new Rol());
    handler->push_back((VirtualCodeHandler *)new Ror());
    handler->push_back((VirtualCodeHandler *)new ShlSal());
    handler->push_back((VirtualCodeHandler *)new Shr());
    handler->push_back((VirtualCodeHandler *)new Loop());
    handler->push_back((VirtualCodeHandler *)new Jmp());
    handler->push_back((VirtualCodeHandler *)new Je());
    handler->push_back((VirtualCodeHandler *)new Jnz());
    handler->push_back((VirtualCodeHandler *)new Js());
    handler->push_back((VirtualCodeHandler *)new Jns());
    handler->push_back((VirtualCodeHandler *)new Jp());
    handler->push_back((VirtualCodeHandler *)new Jnp());
    handler->push_back((VirtualCodeHandler *)new Jo());
    handler->push_back((VirtualCodeHandler *)new Jno());
    handler->push_back((VirtualCodeHandler *)new Jl());
    handler->push_back((VirtualCodeHandler *)new Jge());
    handler->push_back((VirtualCodeHandler *)new Jle());
    handler->push_back((VirtualCodeHandler *)new Jg());
    handler->push_back((VirtualCodeHandler *)new Jb());
    handler->push_back((VirtualCodeHandler *)new Jae());
    handler->push_back((VirtualCodeHandler *)new Jbe());
    handler->push_back((VirtualCodeHandler *)new Ja());
    handler->push_back((VirtualCodeHandler *)new Sete());
    handler->push_back((VirtualCodeHandler *)new Setne());
    handler->push_back((VirtualCodeHandler *)new Sets());
    handler->push_back((VirtualCodeHandler *)new Setns());
    handler->push_back((VirtualCodeHandler *)new Setp());
    handler->push_back((VirtualCodeHandler *)new Setnp());
    handler->push_back((VirtualCodeHandler *)new Seto());
    handler->push_back((VirtualCodeHandler *)new Setno());
    handler->push_back((VirtualCodeHandler *)new Setl());
    handler->push_back((VirtualCodeHandler *)new Setge());
    handler->push_back((VirtualCodeHandler *)new Setle());
    handler->push_back((VirtualCodeHandler *)new Setg());
    handler->push_back((VirtualCodeHandler *)new Setb());
    handler->push_back((VirtualCodeHandler *)new Setae());
    handler->push_back((VirtualCodeHandler *)new Setna());
    handler->push_back((VirtualCodeHandler *)new Seta());
    handler->push_back((VirtualCodeHandler *)new Adc());
    handler->push_back((VirtualCodeHandler *)new Add());
    handler->push_back((VirtualCodeHandler *)new Sbb());
    handler->push_back((VirtualCodeHandler *)new Sub());
    handler->push_back((VirtualCodeHandler *)new Cmp());
    handler->push_back((VirtualCodeHandler *)new Lea());
    handler->push_back((VirtualCodeHandler *)new SetNewSEH());
    handler->push_back((VirtualCodeHandler *)new Movzx());
    handler->push_back((VirtualCodeHandler *)new Movsx());
    handler->push_back((VirtualCodeHandler *)new Xchg());
    handler->push_back((VirtualCodeHandler *)new Xor());
    handler->push_back((VirtualCodeHandler *)new And());
    handler->push_back((VirtualCodeHandler *)new Test());
    handler->push_back((VirtualCodeHandler *)new Or());
    handler->push_back((VirtualCodeHandler *)new Push());
    handler->push_back((VirtualCodeHandler *)new Pop());
    handler->push_back((VirtualCodeHandler *)new Pushf());
    handler->push_back((VirtualCodeHandler *)new Popf());
    handler->push_back((VirtualCodeHandler *)new Pushad());
    handler->push_back((VirtualCodeHandler *)new Popad());
    handler->push_back((VirtualCodeHandler *)new Call());
    handler->push_back((VirtualCodeHandler *)new Exit());
    handler->push_back((VirtualCodeHandler *)new Jcxz());
    handler->push_back((VirtualCodeHandler *)new Jecxz());
    handler->push_back((VirtualCodeHandler *)new MUL());
    handler->push_back((VirtualCodeHandler *)new H231());
    handler->push_back((VirtualCodeHandler *)new H232());
}

ULONG_PTR VM::DevirtualizeExternal(ULONG_PTR iMod,ULONG_PTR IntDest)
{
    lModInfo keyJumpSec;
    lModInfo vDll;
    lModInfo cSec;
    MEMORY_BASIC_INFORMATION ByteCodeRegion;
    ULONG_PTR initPattern;
    ULONG_PTR areaStart;
    ULONG_PTR byteCode;
    Relocater reloc;
    MEMORY_BASIC_INFORMATION mbInf;

    //cSec.lModVA = (ULONG_PTR)GetModuleHandleA((LPCSTR)iMod);

    if (iMod==0)
        cSec.lModVA = (ULONG_PTR)GetModuleHandleA(0);
    else
        cSec.lModVA=iMod;

    VirtualQuery((LPCVOID)cSec.lModVA, &mbInf, sizeof(MEMORY_BASIC_INFORMATION));
    cSec.lModSize = mbInf.RegionSize;

    keyJumpSec = GetSectionBase(iMod,-1);
    vDll = GetSectionBase(iMod,-2);
    FindVM(vDll);

    ULONG_PTR vmAddr = GetVMAddrFromDispatcher(dispatcher[0]);
    DetectVMVersion(vmAddr);
    pBYTECODE=FindByteCode(vmAddr);

    if(pBYTECODE == 0xDEADC0DE) //byte code not found
        return 0;

    if (VM_Version>=3080)
        FindPCodeTable((LPVOID)vmAddr);

    if (dispatcher[0]< 1000)
        return false;





    VirtualQuery((LPCVOID)pBYTECODE, &ByteCodeRegion, sizeof(MEMORY_BASIC_INFORMATION));
    //------------Handle Decompiled Code Dest------
    if (IntDest==0)
    {
        pDecompiledCode=pBYTECODE;

    }
    else if (IntDest==1)
    {
        pDecompiledCode=0;
        /*while (pDecompiledCode<(ULONG_PTR)mbInf.BaseAddress)
            pDecompiledCode=(ULONG_PTR)VirtualAlloc(0,ByteCodeRegion.RegionSize,0x3000,0x40);*/
        pDecompiledCode=AllocateMem(dispatcher[0],ByteCodeRegion.RegionSize);
        if (pDecompiledCode==0)
        {
            while (pDecompiledCode<(ULONG_PTR)mbInf.BaseAddress)
                pDecompiledCode=(ULONG_PTR)VirtualAlloc(0,ByteCodeRegion.RegionSize,0x3000,0x40);

        }
    }
    else
    {
        pDecompiledCode=IntDest;
    }
    //-------------------

    LogVParts(*entries, pBYTECODE,keyJumpSec,0);        //false: External
    if(!entries->size())
        return 0;

    if (VM_Version<3080)
        ConvertByteCode(*entries, *RelocMapKey, *RelocMapAddr, *KeyMap, cSec);
    else
    {
        if (VM_Version>=3110)
            ConvertByteCodeNew(*entries, *RelocMapKey, *RelocMapAddr, *KeyMap, cSec,false);
        else
            ConvertByteCodeNew(*entries, *RelocMapKey, *RelocMapAddr, *KeyMap, cSec,true);
    }
    //---------


    //val2 = ConvertFileOffsetToVA(FileMapVA, val, true);

    //--------------------------
    reloc.FixEntries(*entries, *RelocMapAddr, *KeyMap, modInfo.lModVA);
    reloc.ConvertKeyRefs(*RelocMapKey, *KeyMap);
    reloc.Relocate(*RelocMapKey, *RelocMapAddr, keyJumpSec.lModVA);

    return entries->size();
}

ULONG_PTR VM::DevirtualizeInternal(ULONG_PTR iMod)
{
    ULONG_PTR byteCode;
    Relocater reloc;
    lModInfo vDll;


    if (iMod==0)
        iMod=(ULONG_PTR)GetModuleHandleA(0);

    modInfo.lModVA=iMod;
    vDll = GetSectionBase(iMod,-2);
    modInfo=vDll;

    ULONG_PTR vmAddr = GetVMAddrFromDispatcher(dispatcher[1]);

    if (!FindVM(vDll)) return false;
    ULONG_PTR dispatcherr = FindDispatcher(dispatcher[1]);
    int NumberOfHandler = DetectVMVersion(vmAddr);


    //-------------------

    if (NumberOfHandler==0)
        return false;
    byteCode = FindByteCode(dispatcherr);
    //byteCode=vDll.lModVA;
    pBYTECODE=byteCode;
    //FindXORValue(dispatcherr);
    pDecompiledCode=pBYTECODE;
    pDecompiledCode=(ULONG_PTR)VirtualAlloc(0,0x100000,0x3000,0x40);
    if (VM_Version>=3080)
        FindPCodeTable((LPVOID)vmAddr);

    int esize = entries->size();

    if (VM_Version<3080)
        ConvertByteCode(*entries, *RelocMapKey, *RelocMapAddr, *KeyMap, modInfo);
    else
    {
        if (VM_Version>=3110)
            ConvertByteCodeNew(*entries, *RelocMapKey, *RelocMapAddr, *KeyMap, modInfo,false);
        else
            ConvertByteCodeNew(*entries, *RelocMapKey, *RelocMapAddr, *KeyMap, modInfo,true);

    }

    LogVParts(*entries, byteCode,vDll, 1, KeyMap->size());
    if(!entries->size())
        return 0;

    reloc.FixEntries(*entries, *RelocMapAddr, *KeyMap, modInfo.lModVA);
    reloc.ConvertKeyRefs(*RelocMapKey, *KeyMap);
    reloc.Relocate(*RelocMapKey, *RelocMapAddr, modInfo.lModVA);

    return entries->size();
}

BOOL VM::FindVM(lModInfo modInfo)
{
    BYTE dispatch[] = {0x60, 0x9C, 0xB2, 0x01, 0xBE, 0x00, 0x00, 0x00, 0x00, 0x8D, 0xBE, 0xcc, 0xcc, 0xcc, 0xcc, 0x8D, 0x3F, 0xB9, 0x01, 0x00, 0x00, 0x00, 0x31, 0xC0, 0xF0, 0x0F, 0xB1, 0x0F, 0x74, 0x0F};
    BYTE wildcard = 0xcc;

    dispatcher[0] = Find((LPVOID)(modInfo.lModVA),modInfo.lModSize,&dispatch,sizeof(dispatch),&wildcard);
    dispatcher[1] = Find((LPVOID)(dispatcher[0] + 1),modInfo.lModSize,&dispatch,sizeof(dispatch),&wildcard);

    if (!dispatcher[0] || !dispatcher[1]) return false;
    else return true;

}

ULONG_PTR VM::FindDispatcher(ULONG_PTR vm)
{
    BYTE dispatcherPattern[] = {0x8B, 0x67, 0x04, 0x57, 0x8D, 0x4F, 0x10, 0x8B, 0x49, 0x10, 0xFF, 0x71, 0xFC, 0xE8};

    return GetJumpDestination(GetCurrentProcess(), Find((LPVOID)vm, 0x100, &dispatcherPattern, sizeof(dispatcherPattern), 0)+ 13);
}


ULONG_PTR VM::FindByteCode(ULONG_PTR dispatcher)
{
    BYTE Pattern_XXXX[] = {0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x00,0x8B,0x40,0x70,0x8B,0x04,0xB0,0x8D,0x04,0xC0,0x8B,0x15,0xCC,0xCC,0xCC,0xCC,0x8B,0x12,0x8B,0x52,0x6C,0x8D,0x1C,0xC2,0x8B,0x03,0x3D,0x31,0x02,0x00,0x00};
    BYTE Pattern_3140_Ext[] = {0x64,0xFF,0x35,0x00,0x00,0x00,0x00,0x64,0x89,0x25,0x00,0x00,0x00,0x00,0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x00,0x8B,0x80,0x80,0x00,0x00,0x00};
    BYTE Pattern_3130_Ext[] = {0x64,0xFF,0x35,0x00,0x00,0x00,0x00,0x64,0x89,0x25,0x00,0x00,0x00,0x00,0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x00,0x8B,0x40,0x7C,0x8B,0x1C,0xB0,0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x00,0x03,0x58,0x78};
    BYTE Pattern_3120_Ext[] = {0x64,0xFF,0x35,0x00,0x00,0x00,0x00,0x64,0x89,0x25,0x00,0x00,0x00,0x00,0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x00,0x8B,0x40,0x6C,0x8B,0x1C,0xB0,0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x00,0x03,0x58,0x68,0x8B,0x03,0x3D,0x31,0x02,0x00,0x00};
    BYTE Pattern_3060_Int[] = {0x64,0xFF,0x30,0x64,0x89,0x20,0x8B,0x5D,0x08,0x33,0xD2,0x55,0x68,0xCC,0xCC,0xCC,0xCC,0x64,0xFF,0x32,0x64,0x89,0x22,0x8D,0x34,0xDB,0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x04,0xF0,0x83,0xC0,0xFD,0x83,0xF8,0x63};


    BYTE Pattern_3140_Int[] = {0x64,0xFF,0x35,0x00,0x00,0x00,0x00,0x64,0x89,0x25,0x00,0x00,0x00,0x00,0xA1,0xCC,0xCC,0xCC,0x00,0x8B,0x1C,0xB0,0x03,0x1D,0xCC,0xCC,0xCC,0xCC,0x8B,0x03,0x3D,0x31,0x02,0x00,0x00};
    BYTE Pattern_3100_Int[] = {0x64,0xFF,0x35,0x00,0x00,0x00,0x00,0x64,0x89,0x25,0x00,0x00,0x00,0x00,0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x04,0xB0,0x8D,0x04,0xC0,0x8B,0x15,0xCC,0xCC,0xCC,0xCC,0x8D,0x1C,0xC2};
    BYTE Pattern_3070_Int[] = {0x64,0xFF,0x32,0x64,0x89,0x22,0x8D,0x34,0xDB,0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x04,0xF0,0x83,0xC0,0xFD,0x83,0xF8,0x63};
    BYTE Pattern_2050_Int[] = {0x64,0xFF,0x30,0x64,0x89,0x20,0x8B,0x5D,0x08,0x33,0xD2,0x55,0x68,0x81,0xFE,0x5A,0x00,0x64,0xFF,0x32,0x64,0x89,0x22,0x8D,0x34,0xDB,0xA1,0xBC,0x27,0x5B,0x00,0x8B,0x04,0xF0,0x83,0xC0,0xFD,0x83,0xF8,0x5E};



    BYTE wildcard = 0xcc;

    ULONG_PTR iPlace = Find((LPVOID)dispatcher,0x200,&Pattern_XXXX,sizeof(Pattern_XXXX),&wildcard ) ;
    ULONG_PTR Result;
    if (iPlace>0)
    {
        __asm
        {
            pushad
            mov eax,iPlace
            add eax,0x1
            mov eax,[eax] //start addr

            mov eax,[eax] //first addr
            mov eax,[eax] //02XXXXXX
            mov eax,[eax+0x6c]
            mov Result,eax
            popad
        }
        pIndexTable=Result;
        VM_Version=3080;
        return Result;
    }

    iPlace = Find((LPVOID)dispatcher,0x200,&Pattern_3140_Ext,sizeof(Pattern_3140_Ext),&wildcard ) ;
    if (iPlace>0)
    {
        __asm
        {
            pushad
            mov eax,iPlace
            add eax,0x1F
            mov eax,[eax]

            mov eax,[eax]
            mov eax,[eax]
            mov eax,[eax+0x7C]
            mov Result,eax
            popad
        }

        VM_Version=3140;
        return Result;
    }


    iPlace = Find((LPVOID)dispatcher,0x200,&Pattern_3130_Ext,sizeof(Pattern_3130_Ext),&wildcard ) ;
    if (iPlace>0)
    {
        __asm
        {
            pushad
            mov eax,iPlace
            add eax,0x1C
            mov eax,[eax]

            mov eax,[eax]
            mov eax,[eax]
            mov eax,[eax+0x78]
            mov Result,eax
            popad
        }

        VM_Version=3130;
        return Result;
    }


    iPlace = Find((LPVOID)dispatcher,0x200,&Pattern_3120_Ext,sizeof(Pattern_3120_Ext),&wildcard ) ;
    if (iPlace>0)
    {
        __asm
        {
            pushad
            mov eax,iPlace
            add eax,0x1C
            mov eax,[eax]

            mov eax,[eax]
            mov eax,[eax]
            mov eax,[eax+0x68]
            mov Result,eax
            popad
        }

        VM_Version=3120;
        return Result;
    }

    iPlace = Find((LPVOID)dispatcher,0x200,&Pattern_3100_Int,sizeof(Pattern_3100_Int),&wildcard ) ;
    if (iPlace>0)
    {
        __asm
        {
            pushad

            mov eax,iPlace
            add eax,0x1B
            mov eax,[eax]
            mov eax,[eax]

            mov Result,eax
            popad
        }
        VM_Version=3100;
        return Result;
    }

    iPlace = Find((LPVOID)dispatcher,0x200,&Pattern_3140_Int,sizeof(Pattern_3140_Int),&wildcard ) ;
    if (iPlace>0)
    {
        __asm
        {
            pushad

            mov eax,iPlace
            add eax,0x18
            mov eax,[eax]
            mov eax,[eax]

            mov Result,eax
            popad
        }
        VM_Version=3140;
        return Result;
    }


    iPlace = Find((LPVOID)dispatcher,0x200,&Pattern_3060_Int,sizeof(Pattern_3060_Int),&wildcard ) ;
    if (iPlace>0)
    {
        __asm
        {
            pushad

            mov eax,iPlace
            add eax,0x1B
            mov eax,[eax]
            mov eax,[eax]

            mov Result,eax
            popad
        }
        VM_Version=3060;
        return Result;
    }

    iPlace = Find((LPVOID)dispatcher,0x200,&Pattern_2050_Int,sizeof(Pattern_2050_Int),&wildcard ) ;
    if (iPlace>0)
    {
        __asm
        {
            pushad

            mov eax,iPlace
            add eax,0x1B
            mov eax,[eax]
            mov eax,[eax]

            mov Result,eax
            popad
        }
        VM_Version=3050;
        return Result;
    }

    iPlace = Find((LPVOID)dispatcher,0x200,&Pattern_3070_Int,sizeof(Pattern_3070_Int),&wildcard ) ;
    if (iPlace>0)
    {
        __asm
        {
            pushad

            mov eax,iPlace
            add eax,0xA
            mov eax,[eax]
            mov eax,[eax]

            mov Result,eax
            popad
        }
        VM_Version=3070;
        return Result;
    }

    return 0xDEADC0DE;
}
void VM::LogVParts(std::map<ULONG_PTR, refs> &refMap, ULONG_PTR bytecode, lModInfo modInfo, int mode,int CodeCount)        //mode = false: External, mode = true: Internal
{
    ULONG_PTR *tPTR;
    BYTE jmppush[] = {0x68, 0xcc, 0xcc, 0xcc, 0xcc, 0xe9};
    BYTE jmpwildcard = 0xcc;
    ULONG_PTR jmppushptr;
    int partcount = 0;
    ULONG_PTR pushval;
    ULONG_PTR pushvalptr;
    refs references;
    ULONG_PTR dispatchAddr = dispatcher[mode];
    FindXORValue(dispatchAddr);
    jmppushptr = modInfo.lModVA - 1;

    do
    {
        jmppushptr = Find((LPVOID)(jmppushptr+1), modInfo.lModSize-(jmppushptr-modInfo.lModVA)-1, &jmppush, sizeof(jmppush), &jmpwildcard);
        ULONG_PTR jmpdest = GetJumpDestination(GetCurrentProcess(),jmppushptr+5);
        if(jmpdest == dispatchAddr)
        {
            pushvalptr = jmppushptr + 1;
            tPTR=(ULONG_PTR*)pushvalptr;
            //*tPTR^=pushXOR;
            pushval = *(ULONG_PTR*)pushvalptr;
            pushval^=pushXOR;
            if (!(pushval>=CodeCount && CodeCount!=0))
            {
                references.addr =  CalcByteCodeVAfromPushVal(bytecode,pushval);
                references.key = pushval;
                refMap.insert(std::make_pair(jmppushptr, references));
            }
        }
    }
    while(jmppushptr != 0);
}

ULONG_PTR VM::CalcByteCodeVAfromPushVal(ULONG_PTR bytecode, ULONG_PTR valu)
{
    ULONG_PTR calcval;
    //valu^=pushXOR;
//    calcval = valu * 9;
    //calcval = bytecode + calcval * 8;
    calcval=valu*4 + bytecode;
    return calcval;
}

void VM::ConvertByteCode(std::map<ULONG_PTR,struct refs> &entrylist, std::map<ULONG_PTR,ULONG_PTR> &RelocMapKey, std::map<ULONG_PTR, ULONG_PTR> &RelocMapAddr, std::map<int, ULONG_PTR> &KeyMap, lModInfo mInf)
{
    Relocater reloc;
    ULONG_PTR bytecode;
    ULONG_PTR entry;
    ULONG_PTR assemblePtr;
    BYTE bla[0x10];
    BYTE *destbuff = bla;
    size_t cSize;
    unsigned int currentindex = 0;
    int key = 0;
    int typeCount = 0;
    bool CodeHandled=false;
    int relocOffset = 0;
    ULONG_PTR relocKey = 0;
    ULONG_PTR relocAddr = 0;
    pSize=0;
    //entrylist.begin()->second.addr=pBYTECODE;
    //assemblePtr = bytecode = entrylist.begin()->second.addr;
    bytecode = pBYTECODE;
    // assemblePtr=pBYTECODE;
    assemblePtr=pDecompiledCode;

    entry = entrylist.begin()->first;

    currentindex = *((unsigned int*)bytecode);

    while(currentindex >= 3 && currentindex <= 0x660)
    {
        CodeHandled=false;
        for(std::vector<VirtualCodeHandler*>::const_iterator n = handler->begin(); n != handler->end(); n++)
        {
            if((*n)->HandleBytecode((unsigned char *)bytecode, currentindex, (unsigned char *)destbuff, relocOffset, relocKey, relocAddr, mInf))
            {
                destbuff = (BYTE*)(*n)->getCode();
                cSize = (*n)->getSize();
                //-------Force Fix IMUL R16,32
                if (currentindex==0x9)
                {
                    if ((destbuff[0]==0x66) && (destbuff[1]==0xF7) && (destbuff[2]>=0xE8) && (destbuff[2]<=0xEF))
                        cSize=3;
                    if ((destbuff[0]==0xF7) && (destbuff[1]>=0xE8) && (destbuff[1]<=0xEF) )
                        cSize=2;

                }


                //------------------------------

                KeyMap[key] = assemblePtr;
                key++;
                CodeHandled=true;
                memcpy((void *)assemblePtr, destbuff, cSize);
                pSize+=cSize;

                if (relocOffset && relocKey) RelocMapKey[assemblePtr + relocOffset] = (ULONG_PTR)relocKey;
                if (relocOffset && relocAddr) RelocMapAddr[assemblePtr + relocOffset] = (ULONG_PTR)relocAddr;

                relocOffset = 0;
                relocKey = 0;
                relocAddr = 0;

                assemblePtr += cSize;

                break;
            }
        }

        if (CodeHandled==false)
        {
            key++;
        }
        bytecode += 0x48;                // instruct size
        currentindex = *((unsigned int*)bytecode);
    }


}
void VM::ConvertByteCodeNew(std::map<ULONG_PTR,struct refs> &entrylist, std::map<ULONG_PTR,ULONG_PTR> &RelocMapKey, std::map<ULONG_PTR, ULONG_PTR> &RelocMapAddr, std::map<int, ULONG_PTR> &KeyMap, lModInfo mInf,bool IndexOrRVA)
{
    Relocater reloc;
    ULONG_PTR bytecode;
    ULONG_PTR entry;
    ULONG_PTR assemblePtr;
    BYTE bla[0x10];
    BYTE *destbuff = bla;
    size_t cSize;

    //--------------------
    struct RedirectRecord
    {
        ULONG_PTR VAddr;
        ULONG_PTR Key;

    };
    std::vector<RedirectRecord> RedirectTable;
    RedirectTable.reserve(10000);
    RedirectRecord iRedirect;
    //--------------------
    unsigned int currentindex = 0;
    int key = 0;
    int typeCount = 0;
    bool CodeHandled=false;
    int relocOffset = 0;
    ULONG_PTR relocKey = 0;
    ULONG_PTR relocAddr = 0;
    pSize=0;
    bytecode = pBYTECODE;
    assemblePtr=pDecompiledCode;
    // ULONG_PTR IndexTable=0;
    ULONG_PTR ptIndexTable=this->pIndexTable;
    ULONG_PTR bytecodeBase=bytecode;

    entry = entrylist.begin()->first;

    __asm
    {
        pushad
        mov ecx,key
        mov eax,ptIndexTable
        mov ebx,[ecx*4+eax]
        mov eax,bytecodeBase
        cmp IndexOrRVA,0
        je DontMul
        imul ebx,ebx,0x48
        DontMul:
        add eax,ebx
        mov bytecode,eax
        mov eax,[eax]
        mov currentindex,eax
        popad
    }

    while(currentindex >= 3 && currentindex <= 0x232 )
    {
        if (key==0x3712)
            int a=0;

        CodeHandled=false;
        for(std::vector<VirtualCodeHandler*>::const_iterator n = handler->begin(); n != handler->end(); n++)
        {
            if((*n)->HandleBytecode((unsigned char *)bytecode, currentindex, (unsigned char *)destbuff, relocOffset, relocKey, relocAddr, mInf))
            {
                if (currentindex==0x232)
                {
                    iRedirect.VAddr=assemblePtr+(*n)->getSize();
                    iRedirect.Key=*PDWORD(bytecode+0x1C);
                    RedirectTable.push_back(iRedirect);
                    (*n)->db(0xE8);
                    (*n)->nop();
                    (*n)->nop();
                    (*n)->nop();
                    (*n)->nop();
                }
                destbuff = (BYTE*)(*n)->getCode();
                cSize = (*n)->getSize();
                //-------Force Fix IMUL R16,32
                if (currentindex==0x9)
                {
                    if ((destbuff[0]==0x66) && (destbuff[1]==0xF7) && (destbuff[2]>=0xE8) && (destbuff[2]<=0xEF))
                        cSize=3;
                    if ((destbuff[0]==0xF7) && (destbuff[1]>=0xE8) && (destbuff[1]<=0xEF) )
                        cSize=2;

                }
                //------------------------------

                KeyMap[key] = assemblePtr;
                key++;
                CodeHandled=true;
                memcpy((void *)assemblePtr, destbuff, cSize);
                pSize+=cSize;

                if (relocOffset && relocKey) RelocMapKey[assemblePtr + relocOffset] = (ULONG_PTR)relocKey;
                if (relocOffset && relocAddr) RelocMapAddr[assemblePtr + relocOffset] = (ULONG_PTR)relocAddr;

                relocOffset = 0;
                relocKey = 0;
                relocAddr = 0;

                assemblePtr += cSize;





                break;
            }
        }

        if (CodeHandled==false)
        {
            key++;
        }


        __asm
        {
            pushad
            mov ecx,key
            mov eax,ptIndexTable
            mov ebx,[ecx*4+eax]
            cmp IndexOrRVA,0
            je DontMul2
            imul ebx,ebx,0x48
            DontMul2:
            mov eax,bytecodeBase
            add eax,ebx
            mov bytecode,eax
            push 0x48
            PUSH EAX
            mov eax,IsBadReadPtr
            call eax
            cmp eax,1
            mov eax,0xFFFFFFFF
            je skipRead;
            mov eax,bytecode
            mov eax,[eax]
            skipRead:
            mov currentindex,eax
            popad

        }

    }

    //--------Fix Redirection
    for (int i=0; i<RedirectTable.size(); i++)
    {
        *PDWORD(RedirectTable[i].VAddr+1)=KeyMap[RedirectTable[i].Key]-RedirectTable[i].VAddr-5;
    }
    //------------------------

}

lModInfo VM::GetSectionBase(ULONG_PTR iMod,int secNum)
{
    HMODULE imagebase;
    WORD nrOfSec;
    HMODULE base;
    lModInfo modInfo;

    if (iMod==0)
        imagebase = GetModuleHandleA((LPCSTR)iMod);
    else
        imagebase=(HMODULE)iMod;


    if (secNum < 0)
    {
        nrOfSec = GetPE32DataFromMappedFile((ULONG_PTR)imagebase, NULL, UE_SECTIONNUMBER);
        secNum = nrOfSec + secNum;
    }

    modInfo.lModVA = (ULONG_PTR)GetPE32DataFromMappedFile((ULONG_PTR)imagebase, secNum, UE_SECTIONVIRTUALOFFSET);
    modInfo.lModVA += (ULONG_PTR)imagebase;
    modInfo.lModSize = (ULONG_PTR)GetPE32DataFromMappedFile((ULONG_PTR)imagebase, secNum , UE_SECTIONVIRTUALSIZE);
    return modInfo;
}

const ULONG_PTR VM::GetVMAddrFromDispatcher(ULONG_PTR dispatcher)
{
    BYTE findPattern[] = {0x8D, 0x4F, 0x10, 0x8B, 0x49, 0x10, 0xFF, 0x71, 0xFC};

    ULONG_PTR VMCall = Find((LPVOID)dispatcher,0x200,&findPattern,sizeof(findPattern),0) + sizeof(findPattern);
    ULONG_PTR VMAddr = GetJumpDestination(GetCurrentProcess(), VMCall);

    return VMAddr;
}

int VM::DetectVMVersion(ULONG_PTR vMAddr)
{
    BYTE findPattern[] = {0x83, 0xC0, 0xFD};
    BYTE findPattern38[] = {0x3D,0x31,0x2,0x0,0x0,0x75,0x37};
    BYTE WildCard=0xAA;


    ULONG_PTR HandlerPtr ;
    if( Find((LPVOID)vMAddr,0x100,&findPattern38,sizeof(findPattern),&WildCard))
    {
        VM_Version=3080;
    }
    else
    {
        VM_Version=0;
    };

    HandlerPtr= Find((LPVOID)vMAddr,0x100,&findPattern,sizeof(findPattern),0) + sizeof(findPattern) + 2;
    if (HandlerPtr<10)
        return 0;
    BYTE NumberOfHandler = *(BYTE*)HandlerPtr;

    return (int)NumberOfHandler + 3;
}

void VM::FindXORValue(ULONG_PTR dispatcher)
{
    ULONG_PTR vmAddr = GetVMAddrFromDispatcher(dispatcher);
    BYTE Pattern_31XX_Ex[] = {0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x00,0x8B,0xB0,0xCC,0xCC,0xCC,0xCC,0x33,0x75,0x08};
    //BYTE Pattern_3140_Ex[] = {0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x00,0x8B,0xB0,0x28,0x05,0x00,0x00,0x33,0x75,0x08};
    //BYTE Pattern_3120_Ex[] = {0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x00,0x8B,0xB0,0x18,0x05,0x00,0x00,0x33,0x75,0x08};
    BYTE Pattern_3070_Ex[] = {0x64,0xFF,0x30,0x64,0x89,0x20,0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x00,0x8B,0x98,0xF0,0x04,0x00,0x00,0x33,0x5D,0x08,0x33,0xD2,0x55};

    BYTE Pattern_3140_Int[] = {0x8B,0x75,0x08,0x33,0x35,0xCC,0xCC,0xCC,0xCC,0x55};
    BYTE Pattern_3100_Int[] = {0x64,0xFF,0x30,0x64,0x89,0x20,0x8B,0x75,0x08,0x33,0x35,0xC0,0x27,0xBB,0x00,0x55};
    BYTE Pattern_3070_Int[] = {0x55,0x68,0xCC,0xCC,0xCC,0xCC,0x64,0xFF,0x30,0x64,0x89,0x20,0x8B,0x5D,0x08,0x33,0x1D,0xCC,0xCC,0xCC,0xCC,0x33,0xD2,0x55};

    PDWORD PtrDWORD;
    BYTE wildcard = 0xcc;
    ULONG_PTR iPlace=0;

    //----------Enigma 3.1XX External
    iPlace = Find((LPVOID)vmAddr,0x100,&Pattern_31XX_Ex,sizeof(Pattern_31XX_Ex),&wildcard );

    if (iPlace>0)
    {
        PtrDWORD=PDWORD(iPlace+1);
        PtrDWORD=(PDWORD)*PtrDWORD;
        PtrDWORD=(PDWORD)*PtrDWORD;
        PtrDWORD=(PDWORD)(*PtrDWORD+*PDWORD(iPlace+9));
        pushXOR=*PtrDWORD;
        return;
    }

    iPlace = Find((LPVOID)vmAddr,0x100,&Pattern_3140_Int,sizeof(Pattern_3140_Int),&wildcard );

    if (iPlace>0)
    {
        PtrDWORD=PDWORD(iPlace+5);
        PtrDWORD=(PDWORD)*PtrDWORD;
        pushXOR=*PtrDWORD;
        return;
    }
    iPlace = Find((LPVOID)vmAddr,0x100,&Pattern_3100_Int,sizeof(Pattern_3100_Int),&wildcard );

    if (iPlace>0)
    {
        PtrDWORD=PDWORD(iPlace+0xB);
        PtrDWORD=(PDWORD)*PtrDWORD;
        pushXOR=*PtrDWORD;
        return;
    }

    iPlace = Find((LPVOID)vmAddr,0x100,&Pattern_3070_Int,sizeof(Pattern_3070_Int),&wildcard );

    if (iPlace>0)
    {
        PtrDWORD=PDWORD(iPlace+0x11);
        PtrDWORD=(PDWORD)*PtrDWORD;
        pushXOR=*PtrDWORD;
        return;
    }

    iPlace = Find((LPVOID)vmAddr,0x100,&Pattern_3070_Ex,sizeof(Pattern_3070_Ex),&wildcard );

    if (iPlace>0)
    {
        PtrDWORD=PDWORD(iPlace+0x7);
        PtrDWORD=(PDWORD)*PtrDWORD;
        PtrDWORD=(PDWORD)*PtrDWORD;
        PtrDWORD=(PDWORD)(*PtrDWORD+0x4F0);;
        pushXOR=*PtrDWORD;
        return;
    }
    pushXOR=0;
}

ULONG_PTR VM::AllocateMem(ULONG_PTR iBase,ULONG_PTR MemSize)
{
    bool bStart=true;

    ULONG_PTR iMem=0;
    ULONG_PTR AllocatedMem;
    MEMORY_BASIC_INFORMATION iMemInfo;
    MEMORY_BASIC_INFORMATION jMemInfo;
    ZeroMemory(&iMemInfo,sizeof(iMemInfo));
    ZeroMemory(&jMemInfo,sizeof(iMemInfo));


    for (iMem=0; iMem<0x7FFFF000; iMem+=iMemInfo.RegionSize)
    {
        VirtualQuery  ((LPCVOID)iMem, &iMemInfo, sizeof(iMemInfo));
        if (bStart==true)
        {
            CopyMemory(&jMemInfo,&iMemInfo,sizeof(iMemInfo));
            bStart=false;
        }

        if (iMemInfo.State!=65536 && iMemInfo.State!=131072)
        {

            if ((iMem-((ULONG_PTR)jMemInfo.AllocationBase+jMemInfo.RegionSize))>MemSize+0x1000)
            {
                AllocatedMem=(ULONG_PTR)VirtualAlloc((LPVOID)(iMem-MemSize-0x1000),MemSize,0x3000,0x40);
                if (AllocatedMem)
                    return AllocatedMem;
            }

        }

        CopyMemory(&jMemInfo,&iMemInfo,sizeof(iMemInfo));
    }

    return 0;

}
LPVOID VM::FindPCodeTable(LPVOID iDispatcher)
{
    if(VM_Version<3080)
        return 0;
    BYTE Pattern_XXXX[] = {0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x00,0x8B,0x40,0x70,0x8B,0x04,0xB0,0x8D,0x04,0xC0,0x8B,0x15,0xCC,0xCC,0xCC,0xCC,0x8B,0x12,0x8B,0x52,0x6C,0x8D,0x1C,0xC2,0x8B,0x03,0x3D,0x31,0x02,0x00,0x00};
    BYTE Pattern_3140_Ext[] = {0x64,0xFF,0x35,0x00,0x00,0x00,0x00,0x64,0x89,0x25,0x00,0x00,0x00,0x00,0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x00,0x8B,0x80,0x80,0x00,0x00,0x00};
    BYTE Pattern_3130_Ext[] = {0x64,0xFF,0x35,0x00,0x00,0x00,0x00,0x64,0x89,0x25,0x00,0x00,0x00,0x00,0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x00,0x8B,0x40,0x7C,0x8B,0x1C,0xB0,0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x00,0x03,0x58,0x78};
    BYTE Pattern_3120_Ext[] = {0x64,0xFF,0x35,0x00,0x00,0x00,0x00,0x64,0x89,0x25,0x00,0x00,0x00,0x00,0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x00,0x8B,0x40,0x6C,0x8B,0x1C,0xB0,0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x00,0x03,0x58,0x68,0x8B,0x03};


    BYTE Pattern_3140_Int[] = {0x64,0xFF,0x35,0x00,0x00,0x00,0x00,0x64,0x89,0x25,0x00,0x00,0x00,0x00,0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x1C,0xB0,0x03,0x1D,0xCC,0xCC,0xCC,0xCC,0x8B,0x03,0x3D,0x31,0x02,0x00,0x00};
    BYTE Pattern_3100_Int[] = {0x64,0xFF,0x35,0x00,0x00,0x00,0x00,0x64,0x89,0x25,0x00,0x00,0x00,0x00,0xA1,0xCC,0xCC,0xCC,0xCC,0x8B,0x04,0xB0,0x8D,0x04,0xC0,0x8B,0x15,0xCC,0xCC,0xCC,0xCC,0x8D,0x1C,0xC2};



    BYTE wildcard = 0xcc;

    ULONG_PTR iPlace = Find((LPVOID)iDispatcher,0x200,&Pattern_XXXX,sizeof(Pattern_XXXX),&wildcard ) ;
    ULONG_PTR Result;
    if (iPlace>0)
    {
        __asm
        {
            pushad
            mov eax,iPlace
            add eax,0x1
            mov eax,[eax] //start addr

            mov eax,[eax] //first addr
            mov eax,[eax] //02XXXXXX
            mov eax,[eax+0x70]
            mov Result,eax
            popad
        }
        pIndexTable=Result;
        return (LPVOID)Result;
    }

    iPlace = Find((LPVOID)iDispatcher,0x200,&Pattern_3140_Ext,sizeof(Pattern_3140_Ext),&wildcard ) ;
    if (iPlace>0)
    {
        __asm
        {
            pushad
            mov eax,iPlace
            add eax,0xF
            mov eax,[eax] //start addr

            mov eax,[eax] //first addr
            mov eax,[eax] //02XXXXXX
            mov eax,[eax+0x80]
            mov Result,eax
            popad
        }
        pIndexTable=Result;
        return (LPVOID)Result;
    }

    iPlace = Find((LPVOID)iDispatcher,0x200,&Pattern_3130_Ext,sizeof(Pattern_3130_Ext),&wildcard ) ;
    if (iPlace>0)
    {
        __asm
        {
            pushad
            mov eax,iPlace
            add eax,0xF
            mov eax,[eax]

            mov eax,[eax]
            mov eax,[eax]
            mov eax,[eax+0x7C]
            mov Result,eax
            popad
        }
        pIndexTable=Result;
        return (LPVOID)Result;
    }

    iPlace = Find((LPVOID)iDispatcher,0x200,&Pattern_3120_Ext,sizeof(Pattern_3120_Ext),&wildcard ) ;
    if (iPlace>0)
    {
        __asm
        {
            pushad
            mov eax,iPlace
            add eax,0xF
            mov eax,[eax]

            mov eax,[eax]
            mov eax,[eax]
            mov eax,[eax+0x6C]
            mov Result,eax
            popad
        }
        pIndexTable=Result;
        return (LPVOID)Result;
    }

    iPlace = Find((LPVOID)iDispatcher,0x200,&Pattern_3140_Int,sizeof(Pattern_3140_Int),&wildcard ) ;
    if (iPlace>0)
    {
        __asm
        {
            pushad

            mov eax,iPlace
            add eax,0xF
            mov eax,[eax]
            mov eax,[eax]

            mov Result,eax
            popad
        }
        pIndexTable=Result;
        return (LPVOID)Result;
    }

    iPlace = Find((LPVOID)iDispatcher,0x200,&Pattern_3100_Int,sizeof(Pattern_3100_Int),&wildcard ) ;
    if (iPlace>0)
    {
        __asm
        {
            pushad

            mov eax,iPlace
            add eax,0xF
            mov eax,[eax]
            mov eax,[eax]

            mov Result,eax
            popad
        }
        pIndexTable=Result;
        VM_Version=3100;
        return (LPVOID)Result;
    }

    return (LPVOID)0xDEADC0DE;


}
