#include "EnigmaAnalyzer.h"
#include <fstream>
#include <sstream>

#define MaxHandlerCount 256

CEnigmaAnalyzer::CEnigmaAnalyzer(void)
{
    dwImageBase=0;



    PatternTable = new HandlerRecord[MaxHandlerCount];
    ZeroMemory(PatternTable,sizeof(HandlerRecord)*MaxHandlerCount);
    ULONG_PTR &i=HandlerCount;
    i=0;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_AL_Mem8_EAXImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=11;
    PatternTable[i].HandlerID=SUB_1_None_Imm32;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOVZX_EAX_Mem8_EAXImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=7;
    PatternTable[i].HandlerID=SUB_1_None_Imm8;
    i++;


    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_AL_Mem8_EAXImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=19;
    PatternTable[i].HandlerID=SUB_1_Single_Imm32;
    i++;


    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=11;
    PatternTable[i].HandlerID=SUB_1_Single_Reg;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOVZX_EAX_Mem16_EAXImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=15;
    PatternTable[i].HandlerID=SUB_1_Single_Reg_Imm16,
                    i++;



    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::ADD_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=19;
    PatternTable[i].HandlerID=SUB_1_Double_REG;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOVZX_EAX_Mem8_EAXImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=15;
    PatternTable[i].HandlerID=SUB_1_Single_Imm8;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::ADD_EBX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_AL_Mem8_EAXImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::ADD_EBX_EAX;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBXImm;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=19;
    PatternTable[i].HandlerID=SUB_1_Single_Mem32_Reg_Imm;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::ADD_EBX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_AL_Mem8_EAXImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::ADD_EBX_EAX;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_AL_Mem8_EBXImm;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=19;
    PatternTable[i].HandlerID=SUB_1_Single_Mem8_Reg_Imm32;
    i++;
    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::ADD_EBX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_AL_Mem8_EAXImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::ADD_EBX_EAX;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOVZX_EAX_Mem16_EBXImm;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=19;
    PatternTable[i].HandlerID=SUB_1_Single_Mem16_Reg_Imm32;
    i++;
    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOVZX_EDX_Mem8_EDXImm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::ADD_EAX_EDX;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOVZX_EAX_Mem16_EAXImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=15;
    PatternTable[i].HandlerID=SUB_1_Single_Mem16_Reg_Imm8;
    i++;
    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOVZX_EDX_Mem8_EDXImm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::ADD_EAX_EDX;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=15;
    PatternTable[i].HandlerID=SUB_1_Single_Mem32_Reg_Imm8;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOVZX_EDX_Mem8_EDXImm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::ADD_EAX_EDX;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOVZX_EAX_Mem8_EAXImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=15;
    PatternTable[i].HandlerID=SUB_1_Single_Mem8_Reg_Imm8;
    i++;


    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::ADD_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOVZX_EAX_Mem8_EAXImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=20;
    PatternTable[i].HandlerID=SUB_1_Double_Mem8_Reg;
    i++;


    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=12;
    PatternTable[i].HandlerID=SUB_1_Single_Mem32_Reg;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOVZX_EAX_Mem16_EAXImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=12;
    PatternTable[i].HandlerID=SUB_1_Single_Mem16_Reg;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOVZX_EAX_Mem8_EAXImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=12;
    PatternTable[i].HandlerID=SUB_1_Single_Mem8_Reg;
    i++;


    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::ADD_EBX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_AL_Mem8_EAXImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::ADD_EBX_EAX;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBXImm;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=27;
    PatternTable[i].HandlerID=SUB_1_Double_Mem32_Reg_Imm;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::ADD_EBX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_AL_Mem8_EAXImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::ADD_EBX_EAX;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::MOVZX_EAX_Mem16_EBXImm;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=27;
    PatternTable[i].HandlerID=SUB_1_Double_Mem16_Reg_Imm;
    i++;


    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::MOVZX_EDX_Mem8_EDXImm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::ADD_EAX_EDX;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOVZX_EAX_Mem8_EAXImm;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=23;
    PatternTable[i].HandlerID=SUB_1_Double_Mem8_Reg_Imm8;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::ADD_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=20;
    PatternTable[i].HandlerID=SUB_1_Double_Mem32_Reg;
    i++;


    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::MOVZX_EDX_Mem8_EDXImm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::ADD_EAX_EDX;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=23;
    PatternTable[i].HandlerID=SUB_1_Double_Mem32_Reg_Imm8;
    i++;


    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::MOVZX_EDX_Mem8_EDXImm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::ADD_EAX_EDX;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOVZX_EAX_Mem16_EAXImm;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=23;
    PatternTable[i].HandlerID=SUB_1_Double_Mem16_Reg_Imm8;
    i++;



    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::ADD_EAX_Mem32_EDXImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::ADD_EBX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_AL_Mem8_EAXImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::ADD_EBX_EAX;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::MOV_AL_Mem8_EBXImm;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=27;
    PatternTable[i].HandlerID=SUB_1_Double_Mem8_Reg_Imm32;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_AL_Mem8_EAXImm;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_Mem8_EBPImm_AL;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=5;
    PatternTable[i].HandlerID=SUB_1_X_8;
    i++;
    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::SHR_EAX_Imm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_Mem8_EBPImm_AL;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=6;
    PatternTable[i].HandlerID=SUB_1_X_8High;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_AX_Mem16_EAXImm;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_Mem16_EBPImm_AX;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=5;
    PatternTable[i].HandlerID=SUB_1_X_16;
    i++;
    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=5;
    PatternTable[i].HandlerID=SUB_1_X_32;
    i++;



    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_Mem32_EAX_EDX;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=10;
    PatternTable[i].HandlerID=SUB_2_MOV32;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::ADD_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[27]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[28]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[29]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[30]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[31]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[32]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[33]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[34]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[35]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[36]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[37]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=38;
    PatternTable[i].HandlerID=SUB_2_ADD32;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_AX_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::ADD_AX_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem16_EDXImm_AX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[27]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[28]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[29]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[30]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[31]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[32]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[33]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[34]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[35]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[36]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[37]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=38;
    PatternTable[i].HandlerID=SUB_2_ADD16;
    i++;


    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_AL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::ADD_AL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem8_EDXImm_AL;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[27]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[28]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[29]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[30]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[31]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[32]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[33]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[34]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[35]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[36]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[37]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=38;
    PatternTable[i].HandlerID=SUB_2_ADD8;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::INC_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::ADD_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[27]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[28]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[29]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[30]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[31]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[32]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[33]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[34]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[35]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[36]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[37]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[38]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[39]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[40]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=41;
    PatternTable[i].HandlerID=SUB_2_ADC32;
    i++;


    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::INC_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_AX_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::ADD_AX_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_Mem16_EDXImm_AX;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[27]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[28]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[29]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[30]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[31]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[32]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[33]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[34]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[35]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[36]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[37]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[38]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[39]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[40]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=41;
    PatternTable[i].HandlerID=SUB_2_ADC16;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::INC_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_AL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::ADD_AL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_Mem8_EDXImm_AL;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[27]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[28]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[29]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[30]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[31]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[32]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[33]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[34]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[35]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[36]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[37]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[38]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[39]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[40]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=41;
    PatternTable[i].HandlerID=SUB_2_ADC8;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::INC_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::SUB_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[27]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[28]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[29]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[30]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[31]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[32]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[33]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[34]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[35]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[36]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[37]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[38]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=39;
    PatternTable[i].HandlerID=SUB_2_SBB32;
    i++;
    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::INC_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_AX_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::SUB_AX_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_Mem16_EDXImm_AX;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[27]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[28]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[29]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[30]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[31]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[32]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[33]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[34]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[35]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[36]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[37]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[38]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=39;
    PatternTable[i].HandlerID=SUB_2_SBB16;
    i++;
    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::INC_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_AL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::SUB_AL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_Mem8_EDXImm_AL;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[27]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[28]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[29]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[30]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[31]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[32]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[33]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[34]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[35]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[36]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[37]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[38]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=39;
    PatternTable[i].HandlerID=SUB_2_SBB8;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_DX_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_Mem16_EAXImm_DX;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=10;
    PatternTable[i].HandlerID=SUB_2_MOV16;
    i++;
    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_DL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_Mem8_EAXImm_DL;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=10;
    PatternTable[i].HandlerID=SUB_2_MOV8;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::AND_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=27;
    PatternTable[i].HandlerID=SUB_2_AND32;
    i++;


    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_AX_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::AND_AX_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem16_EDXImm_AX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=27;
    PatternTable[i].HandlerID=SUB_2_AND16;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_AL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::AND_AL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem8_EDXImm_AL;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=27;
    PatternTable[i].HandlerID=SUB_2_AND8;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::SUB_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[27]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[28]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[29]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[30]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[31]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[32]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[33]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[34]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[35]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=36;
    PatternTable[i].HandlerID=SUB_2_SUB32;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_AX_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::SUB_AX_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem16_EDXImm_AX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[27]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[28]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[29]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[30]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[31]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[32]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[33]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[34]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[35]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=36;
    PatternTable[i].HandlerID=SUB_2_SUB16;
    i++;
    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_AL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::SUB_AL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem8_EDXImm_AL;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[27]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[28]=CPatternDetection::PatternType::MOV_ECX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[29]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[30]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[31]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[32]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[33]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[34]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[35]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=36;
    PatternTable[i].HandlerID=SUB_2_SUB8;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::OR_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=27;
    PatternTable[i].HandlerID=SUB_2_OR32;
    i++;
    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_AX_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::OR_AX_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem16_EDXImm_AX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=27;
    PatternTable[i].HandlerID=SUB_2_OR16;
    i++;
    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_AL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::OR_AL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem8_EDXImm_AL;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=27;
    PatternTable[i].HandlerID=SUB_2_OR8;
    i++;
    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::XOR_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=27;
    PatternTable[i].HandlerID=SUB_2_XOR32;
    i++;
    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_AX_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::XOR_AX_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem16_EDXImm_AX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=27;
    PatternTable[i].HandlerID=SUB_2_XOR16;
    i++;
    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_AL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::XOR_AL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem8_EDXImm_AL;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[11]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[12]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[13]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[14]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[15]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[16]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[17]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[18]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[19]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[20]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[21]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[22]=CPatternDetection::PatternType::CALL_Imm;
    PatternTable[i].HandlerPattern[23]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[24]=CPatternDetection::PatternType::INC_EAX;
    PatternTable[i].HandlerPattern[25]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[26]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].PatternCount=27;
    PatternTable[i].HandlerID=SUB_2_XOR8;
    i++;


    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::CALL_Mem32_EBP10;
    PatternTable[i].PatternCount=10;
    PatternTable[i].HandlerID=SUB_3_Single;
    i++;
    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_ECX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].PatternCount=3;
    PatternTable[i].HandlerID=SUB_3_NULL;
    i++;



    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=5;
    PatternTable[i].HandlerID=SUB_3_1_32;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::AND_EAX_Imm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOVZX_EDX_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::ADD_EAX_EDX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=9;
    PatternTable[i].HandlerID=SUB_3_1_16;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOVZX_EDX_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::ADD_EAX_EDX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=9;
    PatternTable[i].HandlerID=SUB_3_1_16;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOVZX_EDX_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_EDX;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=8;
    PatternTable[i].HandlerID=SUB_3_1_16;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::AND_EAX_EDI;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOVZX_EDX_Mem16_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::ADD_EAX_EDX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=9;
    PatternTable[i].HandlerID=SUB_3_1_16;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_DL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::ADD_EAX_EDX;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=8;
    PatternTable[i].HandlerID=SUB_3_1_8;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_DL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::SHL_EDX_Imm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::ADD_EAX_EDX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=9;
    PatternTable[i].HandlerID=SUB_3_1_8High;
    i++;

    //---------------------------------Pattern 2
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::AND_EAX_Imm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_DL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::SHL_EDX_Imm;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::ADD_EAX_EDX;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=10;
    PatternTable[i].HandlerID=SUB_3_1_8High;
    i++;
    //---------------------------------Pattern 3
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::AND_EAX_EDI;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::MOV_DL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::SHL_EDX_Imm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::ADD_EAX_EDX;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[10]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=11;
    PatternTable[i].HandlerID=SUB_3_1_8High;
    i++;

    //---------------------------------Pattern 3
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::AND_EAX_EDI;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_DL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::SHL_EDX_Imm;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::ADD_EAX_EDX;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[9]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=10;
    PatternTable[i].HandlerID=SUB_3_1_8High;
    i++;

    //---------------------------------Pattern 4
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::MOV_EDX_Mem32_EDX;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_DL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::ADD_EAX_EDX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=9;
    PatternTable[i].HandlerID=SUB_3_1_8;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::AND_EAX_EDI;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_DL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::ADD_EAX_EDX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=9;
    PatternTable[i].HandlerID=SUB_3_1_8;
    i++;

    //---------------------------------
    PatternTable[i].HandlerPattern[0]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EDX;
    PatternTable[i].HandlerPattern[1]=CPatternDetection::PatternType::MOV_Mem32_EBPImm_EAX;
    PatternTable[i].HandlerPattern[2]=CPatternDetection::PatternType::MOV_EAX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[3]=CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm;
    PatternTable[i].HandlerPattern[4]=CPatternDetection::PatternType::AND_EAX_Imm;
    PatternTable[i].HandlerPattern[5]=CPatternDetection::PatternType::MOV_DL_Mem8_EBPImm;
    PatternTable[i].HandlerPattern[6]=CPatternDetection::PatternType::ADD_EAX_EDX;
    PatternTable[i].HandlerPattern[7]=CPatternDetection::PatternType::MOV_EDX_Mem32_EBPImm;
    PatternTable[i].HandlerPattern[8]=CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX;
    PatternTable[i].PatternCount=9;
    PatternTable[i].HandlerID=SUB_3_1_8;
    i++;

}


CEnigmaAnalyzer::~CEnigmaAnalyzer(void)
{
}
CEnigmaAnalyzer::HandlerType CEnigmaAnalyzer::GetHandlerType(ULONG_PTR VAddr)
{

    if (HandlerTable.find(VAddr)!=HandlerTable.end())
    {
        HandlerInstruction = HandlerTable[VAddr]->HandlerInstruction;
        return HandlerTable[VAddr]->iHandlerID;
    }





    TraceCode(VAddr);
    /*    std::stringstream iStr;

        iStr<<std::hex<<VAddr<<".txt";
        std::fstream iTrace(iStr.str(),std::ios_base::out);



        for (int i=0;i<HandlerInstruction->size();i++)
        {
            iTrace<<std::hex<<(*HandlerInstruction)[i].iDisasm.VirtualAddr;
            iTrace<<"  =>\t"<<(*HandlerInstruction)[i].iDisasm.CompleteInstr<<"\t\t"<<CPatternDetection::GetHandlerString((*HandlerInstruction)[i].iType)<<std::endl;
        }

        iTrace.close();*/


    HandlerType iResult= AnalyzeHandler();

    CacheRecord *iCache = new CacheRecord;

    iCache->HandlerInstruction=HandlerInstruction;
    iCache->iHandlerID=iResult;
    HandlerTable[VAddr]=iCache;

    return iResult;



}

#pragma optimize( "", off)
ULONG_PTR CEnigmaAnalyzer::TraceCode(ULONG_PTR VAddr)
{
    bool LastFlag=false;
    DISASM iDisasm;
    PDWORD ptrD;
    PBYTE ptrB;
    ZeroMemory(&iDisasm,sizeof(iDisasm));
    iDisasm.Archi=32;
    unsigned long iAddr=(ULONG)VAddr;
    iDisasm.EIP=iDisasm.VirtualAddr=iAddr;
    int iCount=0;
    int iSize;
    ULONG_PTR tmp;
    bool Eq;
    bool JEOrJNE;
    HandlerInstruction = new std::vector<InstRecord>;
    (*HandlerInstruction).clear();
    (*HandlerInstruction).reserve(2000);
    CPatternDetection::PatternType iType;
    CPatternDetection PatternEngine;
    InstRecord iRecord;
    ULONG_PTR Context[8];
    std::stack<ULONG_PTR> Stack;

    for (int i=0; i<16; i++)
        Stack.push(i);

    /*if (VAddr==0x00c1f21d)
        int a=0;
        */
    while (iDisasm.Instruction.Opcode!=0xC3 && iDisasm.Instruction.Opcode!=0xC2 && !(iDisasm.Instruction.Opcode==0xFF &&  iDisasm.Instruction.BranchType==JmpType && iDisasm.Argument1.Memory.BaseRegister==ESP))
    {
        iSize=Disasm(&iDisasm);



        iType=PatternEngine.ValidatePattern(iDisasm);

        /*    if (iDisasm.VirtualAddr==0xC08CBA)
                int a=0;*/
        switch (iType)
        {
        case CPatternDetection::CALL_Imm:

            if (*(PDWORD)(iDisasm.Instruction.AddrValue)==0x424648D)
            {
                iDisasm.EIP=iDisasm.Instruction.AddrValue+4;
                iDisasm.VirtualAddr=iDisasm.Instruction.AddrValue+4;
                goto EndSwitch;
            }
            if (iDisasm.Instruction.AddrValue==(iDisasm.VirtualAddr+9)) // Encrypted Const (XOR Used for AND)
            {
                ULONG_PTR OldMem=iDisasm.EIP;
                BYTE opcode[5];
                opcode[0]=0x68;
                *PDWORD(opcode+1)=iDisasm.VirtualAddr + 5;
                iDisasm.EIP =(UIntPtr) opcode;
                Disasm(&iDisasm);
                iSize=9;
                iType=PatternEngine.ValidatePattern(iDisasm);
                iDisasm.EIP = OldMem;

            }


            goto def;
            break;


        case CPatternDetection::CMP_R32_Imm:
            LastFlag=(Context[GetRegIndex(iDisasm.Argument1.ArgType)]==iDisasm.Instruction.Immediat);
            iDisasm.EIP+=iSize;
            iDisasm.VirtualAddr+=iSize;

            break;
        case CPatternDetection::CMP_Mem32Imm_Imm:

            Eq=(*(PDWORD)(iDisasm.Argument1.Memory.Displacement))==iDisasm.Instruction.Immediat;

            /*JEOrJNE=(*(PBYTE)(iDisasm.EIP+iSize+0x1))==0x84;

            if ((Eq && JEOrJNE) || (!Eq && !JEOrJNE))
            {
                iDisasm.EIP+=iSize;
                iDisasm.VirtualAddr+=iSize;
                iSize=Disasm(&iDisasm);
                iDisasm.EIP=iDisasm.Instruction.AddrValue;
                iDisasm.VirtualAddr=iDisasm.Instruction.AddrValue;
                goto EndSwitch;
            }*/
            LastFlag=Eq;

            iDisasm.EIP+=iSize;
            iDisasm.VirtualAddr+=iSize;

            break;


        case CPatternDetection::MOV_R32_Mem32Imm:
            PDWORD iThunk;
            iThunk=(PDWORD)iDisasm.Argument2.Memory.Displacement;
            Context[GetRegIndex(iDisasm.Argument1.ArgType)]=*iThunk;
            iDisasm.VirtualAddr+=iSize;
            iDisasm.EIP+=iSize;

            break;
        //case CPatternDetection::RET:
        //case CPatternDetection::RET_X:
        case CPatternDetection::JMP_Mem32EBP:
            break;

        case CPatternDetection::JE_Imm:
            if (LastFlag)
            {
                iDisasm.EIP=iDisasm.Instruction.AddrValue;
                iDisasm.VirtualAddr=iDisasm.Instruction.AddrValue;
            }
            else
            {
                iDisasm.VirtualAddr+=iSize;
                iDisasm.EIP+=iSize;
            }
            goto EndSwitch;
            break;

        case CPatternDetection::JNE_Imm:
            if (!LastFlag)
            {
                iDisasm.EIP=iDisasm.Instruction.AddrValue;
                iDisasm.VirtualAddr=iDisasm.Instruction.AddrValue;
            }
            else
            {
                iDisasm.VirtualAddr+=iSize;
                iDisasm.EIP+=iSize;
            }
            goto EndSwitch;
            break;

        case CPatternDetection::JMP_Imm:
JMP:

            iDisasm.EIP=iDisasm.Instruction.AddrValue;
            iDisasm.VirtualAddr=iDisasm.Instruction.AddrValue;

            break;


        case CPatternDetection::PUSH_R32:

            Stack.push(Context[GetRegIndex(iDisasm.Argument2.ArgType)]);
            iDisasm.VirtualAddr+=iSize;
            iDisasm.EIP+=iSize;
            break;

        case CPatternDetection::POP_R32:
            Context[GetRegIndex(iDisasm.Argument1.ArgType)]=Stack.top();
            Stack.pop();
            iDisasm.VirtualAddr+=iSize;
            iDisasm.EIP+=iSize;
            break;

        case CPatternDetection::MOV_Mem32_ESP_R32:

            Stack.top()=Context[GetRegIndex(iDisasm.Argument2.ArgType)];
            iDisasm.VirtualAddr+=iSize;
            iDisasm.EIP+=iSize;
            break;

        case CPatternDetection::MOV_R32_Mem32_ESP:
            Context[GetRegIndex(iDisasm.Argument1.ArgType)]=Stack.top();
            iDisasm.VirtualAddr+=iSize;
            iDisasm.EIP+=iSize;
            break;
        case CPatternDetection::LEA_ESP_Mem32_ESP4:
            Stack.pop();
            iDisasm.VirtualAddr+=iSize;
            iDisasm.EIP+=iSize;
            break;
        case CPatternDetection::LEA_ESP_Mem32_ESP_4:
            Stack.push(0);
            iDisasm.VirtualAddr+=iSize;
            iDisasm.EIP+=iSize;
            break;
        case CPatternDetection::PUSHFD:
            Stack.push(LastFlag);
            iDisasm.VirtualAddr+=iSize;
            iDisasm.EIP+=iSize;
            break;
        case CPatternDetection::POPFD:
            LastFlag=Stack.top();
            Stack.pop();
            iDisasm.VirtualAddr+=iSize;
            iDisasm.EIP+=iSize;
            break;

        case CPatternDetection::TEST_AL_AL:
            LastFlag=false;
            iDisasm.VirtualAddr+=iSize;
            iDisasm.EIP+=iSize;
            break;

        default:
def:

            iRecord.iDisasm=iDisasm;
            iRecord.iType=iType;
            (*HandlerInstruction).push_back(iRecord);
            iCount++;
            iDisasm.EIP+=iSize;
            iDisasm.VirtualAddr+=iSize;

        }



EndSwitch:
        ;

    }

    return iCount;



}
#pragma optimize( "", on )
void CEnigmaAnalyzer::GetEnigmaInfo(ULONG_PTR iMod)
{
    HMODULE imagebase;
    WORD nrOfSec;
    HMODULE base;
    int secNum;
    char TempAddr[MAX_PATH];
    if (iMod==0)
        imagebase = GetModuleHandleA((LPCSTR)iMod);
    else
        imagebase=(HMODULE)iMod;


    GetModuleFileNameA(imagebase,TempAddr,MAX_PATH);
    if (GetModuleHandleA(TempAddr)!=imagebase) // its Internal VM
    {
        EnigmaBase=(ULONG_PTR)imagebase;
        dwImageBase=(ULONG_PTR)imagebase;
        return;

    }

    nrOfSec = GetPE32DataFromMappedFile((ULONG_PTR)imagebase, NULL, UE_SECTIONNUMBER);
    secNum = nrOfSec -2;


    EnigmaBase=(ULONG_PTR)GetPE32DataFromMappedFile((ULONG_PTR)imagebase, secNum, UE_SECTIONVIRTUALOFFSET) + (ULONG_PTR)imagebase;
    EnigmaSize= (ULONG_PTR)GetPE32DataFromMappedFile((ULONG_PTR)imagebase, secNum , UE_SECTIONVIRTUALSIZE);
}

CEnigmaAnalyzer::HandlerType CEnigmaAnalyzer::AnalyzeHandler()
{
    CPatternDetection PatternEngine;

    std::vector<CPatternDetection::PatternType> DumpedType;

    for (int i=0; i<(*HandlerInstruction).size(); i++)
    {
        if ((*HandlerInstruction)[i].iType!=CPatternDetection::NotFound && (*HandlerInstruction)[i].iType!=CPatternDetection::RET && (*HandlerInstruction)[i].iType!=CPatternDetection::RET_X)
        {
            if ((*HandlerInstruction)[i].iType==CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm && strcmp((*HandlerInstruction)[i+1].iDisasm.Instruction.Mnemonic,"xor ")==0)
                continue;
            if ((*HandlerInstruction)[i].iType==CPatternDetection::PatternType::MOV_EDX_Mem32_EDX && strcmp((*HandlerInstruction)[i+1].iDisasm.Instruction.Mnemonic,"xor ")==0)
                continue;

            DumpedType.push_back((*HandlerInstruction)[i].iType);
        }
    }


    /*std::fstream iTrace("Pattern.txt",std::ios_base::out);



    for (int i=0;i<DumpedType.size();i++)
    {
        iTrace<<"PatternTable[i].HandlerPattern["<<i<<"]=CPatternDetection::PatternType::"<<CPatternDetection::GetHandlerString(DumpedType[i])<<";"<<std::endl;
    }

    iTrace.close();*/


    for (int i=0; i<HandlerCount; i++)
    {

        if (PatternTable[i].PatternCount!=DumpedType.size())
            continue;
        for (int j=0; j<DumpedType.size(); j++)
        {
            if (PatternTable[i].HandlerPattern[j]!=DumpedType[j])
                goto NextHandler;

        }


        return PatternTable[i].HandlerID;

NextHandler:
        ;
    }

    MessageBoxA(0,"Unable to recognize handle! Report the file to Raham","Error",0);
    return Undifined_Handler;


};



void CEnigmaAnalyzer::AnalyzeOpcode(ULONG_PTR *Stream)
{
    bool S3_1_Need=false;
    CurrentOpcode=NONE;

    Sub_1=Stream[6];
    Sub_2=Stream[7];
    Sub_3=Stream[12];

    Sub_1_1=Stream[2];
    Sub_1_2=Stream[3];
    Sub_3_1=Stream[8];



    Sub_1Type=GetHandlerType(Sub_1+EnigmaBase);
    Sub_3_1_Type=HandlerType::Undifined_Handler;

    //Sub_3Type=GetHandlerType(Sub_3+EnigmaBase);

    //------
    ZeroMemory(&Operand1,sizeof(Operand1));
    ZeroMemory(&Operand2,sizeof(Operand2));


    //---------


    //Analyze First Handler
    switch (Sub_1Type)
    {
    case HandlerType::SUB_1_Double_Mem32_Reg_Imm8:
        Sub_1_1_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand1=AnalyzeOperand(Sub_1_1_Type);
        Operand1.ExtraValue=0;
        Operand1.bMemory=false;
        Sub_1_2_Type=GetHandlerType(Sub_1_2+EnigmaBase);
        Operand2=AnalyzeOperand(Sub_1_2_Type);
        Operand2.ExtraValue=Stream[4]&0xFF;
        Operand2.bMemory=true;
        break;
    case HandlerType::SUB_1_Double_Mem16_Reg_Imm8:
        Sub_1_1_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand1=AnalyzeOperand(Sub_1_1_Type);
        Operand1.ExtraValue=0;
        Operand1.bMemory=false;
        Sub_1_2_Type=GetHandlerType(Sub_1_2+EnigmaBase);
        Operand2=AnalyzeOperand(Sub_1_2_Type);
        Operand2.ExtraValue=Stream[4]&0xFF;
        Operand2.bMemory=true;
        break;
    case HandlerType::SUB_1_Double_Mem8_Reg_Imm32:
        Sub_1_1_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand1=AnalyzeOperand(Sub_1_1_Type);
        Operand1.ExtraValue=0;
        Operand1.bMemory=false;
        Sub_1_2_Type=GetHandlerType(Sub_1_2+EnigmaBase);
        Operand2=AnalyzeOperand(Sub_1_2_Type);
        Operand2.ExtraValue=Stream[4] + dwImageBase*(Stream[5]&1);;
        Operand2.bMemory=true;
        break;
    case HandlerType::SUB_1_Double_Mem8_Reg_Imm8:
        Sub_1_1_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand1=AnalyzeOperand(Sub_1_1_Type);
        Operand1.ExtraValue=0;
        Operand1.bMemory=false;
        Sub_1_2_Type=GetHandlerType(Sub_1_2+EnigmaBase);
        Operand2=AnalyzeOperand(Sub_1_2_Type);
        Operand2.ExtraValue=Stream[4]&0xFF;
        Operand2.bMemory=true;
        break;
    case HandlerType::SUB_1_Double_Mem16_Reg_Imm:
        Sub_1_1_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand1=AnalyzeOperand(Sub_1_1_Type);
        Operand1.ExtraValue=0;
        Operand1.bMemory=false;
        Sub_1_2_Type=GetHandlerType(Sub_1_2+EnigmaBase);
        Operand2=AnalyzeOperand(Sub_1_2_Type);
        Operand2.ExtraValue=Stream[4] + dwImageBase*(Stream[5]&1);
        Operand2.bMemory=true;
        break;
    case HandlerType::SUB_1_Single_Mem32_Reg:
        Sub_1_2_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand2=AnalyzeOperand(Sub_1_2_Type);
        Operand2.ExtraValue=0;
        Operand2.bMemory=true;
        break;
    case HandlerType::SUB_1_Single_Mem8_Reg:
        Sub_1_2_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand2=AnalyzeOperand(Sub_1_2_Type);
        Operand2.ExtraValue=0;
        Operand2.bMemory=true;
        break;
    case HandlerType::SUB_1_Single_Mem16_Reg:
        Sub_1_2_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand2=AnalyzeOperand(Sub_1_2_Type);
        Operand2.ExtraValue=0;
        Operand2.bMemory=true;
        break;
    case HandlerType::SUB_1_Single_Mem8_Reg_Imm8:
        Sub_1_2_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand2=AnalyzeOperand(Sub_1_2_Type);
        Operand2.ExtraValue=Sub_1_2&0xFF;
        Operand2.bMemory=true;
        break;
    case HandlerType::SUB_1_Double_Mem8_Reg:
        Sub_1_1_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand1=AnalyzeOperand(Sub_1_1_Type);

        Sub_1_2_Type=GetHandlerType(Sub_1_2+EnigmaBase);
        Operand2=AnalyzeOperand(Sub_1_2_Type);
        Operand2.ExtraValue=0;
        Operand2.bMemory=true;
        break;
    case HandlerType::SUB_1_Single_Reg_Imm16:// First Operand: Reg ..... Second Operand: Imm
        Sub_1_1_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand1=AnalyzeOperand(Sub_1_1_Type);
        Operand2.bHighReg=false;
        Operand2.OperandSize=16;
        Operand2.RegOrImm=false;
        Operand2.Value=Sub_1_2&0xFFFF;
        Operand2.bMemory=false;
        break;
    case HandlerType::SUB_1_Single_Mem32_Reg_Imm:
        Sub_1_2_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand2=AnalyzeOperand(Sub_1_2_Type);
        Operand2.ExtraValue=Sub_1_2 + dwImageBase*(Stream[4]&1);
        Operand2.bMemory=true;
        break;
    case HandlerType::SUB_1_Single_Mem32_Reg_Imm8:
        Sub_1_2_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand2=AnalyzeOperand(Sub_1_2_Type);
        Operand2.ExtraValue=Sub_1_2&0xFF;
        Operand2.bMemory=true;
        break;
    case HandlerType::SUB_1_Single_Mem8_Reg_Imm32:
        Sub_1_2_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand2=AnalyzeOperand(Sub_1_2_Type);
        Operand2.ExtraValue=Sub_1_2 + dwImageBase*(Stream[4]&1);
        Operand2.bMemory=true;
        break;
    case HandlerType::SUB_1_Single_Mem16_Reg_Imm32:
        Sub_1_2_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand2=AnalyzeOperand(Sub_1_2_Type);
        Operand2.ExtraValue=Sub_1_2 + dwImageBase*(Stream[4]&1);
        Operand2.bMemory=true;
        break;
    case HandlerType::SUB_1_Single_Mem16_Reg_Imm8:
        Sub_1_2_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand2=AnalyzeOperand(Sub_1_2_Type);
        Operand2.ExtraValue=Sub_1_2 & 0xFF;
        Operand2.bMemory=true;
        break;
    case HandlerType::SUB_1_Double_Mem32_Reg_Imm:
        Sub_1_1_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand1=AnalyzeOperand(Sub_1_1_Type);
        Sub_1_2_Type=GetHandlerType(Sub_1_2+EnigmaBase);
        Operand2=AnalyzeOperand(Sub_1_2_Type);
        Operand2.ExtraValue=Stream[4] + dwImageBase*(Stream[5]&1);
        Operand2.bMemory=true;
        break;
    case HandlerType::SUB_1_Double_Mem32_Reg:
        Sub_1_1_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand1=AnalyzeOperand(Sub_1_1_Type);
        Sub_1_2_Type=GetHandlerType(Sub_1_2+EnigmaBase);
        Operand2=AnalyzeOperand(Sub_1_2_Type);
        Operand2.ExtraValue=0;
        Operand2.bMemory=true;
        break;
    case HandlerType::SUB_1_None_Imm32:
        Operand2.OperandSize=32;
        Operand2.RegOrImm=false;
        Operand2.Value=Sub_1_1 + dwImageBase*(Stream[3]&1);
        break;
    case HandlerType::SUB_1_None_Imm8:
        Operand2.OperandSize=8;
        Operand2.RegOrImm=false;
        Operand2.Value=Sub_1_1&0xFF;
        break;
    case HandlerType::SUB_1_Single_Imm32: //First: Reg            Second: Imm
        Sub_1_1_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand1=AnalyzeOperand(Sub_1_1_Type);

        Operand2.OperandSize=32;
        Operand2.RegOrImm=false;
        Operand2.Value=Sub_1_2 + dwImageBase*(Stream[4]&1);

        break;

    case HandlerType::SUB_1_Single_Imm8: // First : Reg        Second: Imm
        Sub_1_1_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand1=AnalyzeOperand(Sub_1_1_Type);

        Operand2.OperandSize=8;
        Operand2.RegOrImm=false;
        Operand2.Value=Sub_1_2&0xFF;


        break;


    case HandlerType::SUB_1_Single_Reg:
        Sub_1_2_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand2=AnalyzeOperand(Sub_1_2_Type);

        break;

    case HandlerType::SUB_1_Double_REG:

        Sub_1_1_Type=GetHandlerType(Sub_1_1+EnigmaBase);
        Operand1=AnalyzeOperand(Sub_1_1_Type);
        Sub_1_2_Type=GetHandlerType(Sub_1_2+EnigmaBase);
        Operand2=AnalyzeOperand(Sub_1_2_Type);
        break;
    }




    //Analyze Second Handler
    Sub_2Type=GetHandlerType(Sub_2+EnigmaBase);
    switch (Sub_2Type)
    {
    case HandlerType::SUB_2_MOV32:
        CurrentOpcode=MOV;
        break;
    case HandlerType::SUB_2_MOV16:
        CurrentOpcode=MOV;
        break;
    case HandlerType::SUB_2_MOV8:
        CurrentOpcode=MOV;
        break;
    case HandlerType::SUB_2_ADD32:
        CurrentOpcode=ADD;
        break;
    case HandlerType::SUB_2_ADD16:
        CurrentOpcode=ADD;
        break;
    case HandlerType::SUB_2_ADD8:
        CurrentOpcode=ADD;
        break;
    case HandlerType::SUB_2_AND32:
        CurrentOpcode=AND;
        break;
    case HandlerType::SUB_2_AND16:
        CurrentOpcode=AND;
        break;
    case HandlerType::SUB_2_AND8:
        CurrentOpcode=AND;
        break;
    case HandlerType::SUB_2_SUB32:
        CurrentOpcode=SUB;
        break;
    case HandlerType::SUB_2_SUB16:
        CurrentOpcode=SUB;
        break;
    case HandlerType::SUB_2_SUB8:
        CurrentOpcode=SUB;
        break;
    case HandlerType::SUB_2_XOR32:
    case HandlerType::SUB_2_XOR16:
    case HandlerType::SUB_2_XOR8:
        CurrentOpcode=XOR;
        break;
    case HandlerType::SUB_2_OR32:
    case HandlerType::SUB_2_OR16:
    case HandlerType::SUB_2_OR8:
        CurrentOpcode=OR;
        break;
    case HandlerType::SUB_2_ADC32:
    case HandlerType::SUB_2_ADC16:
    case HandlerType::SUB_2_ADC8:
        CurrentOpcode=ADC;
        break;
    case HandlerType::SUB_2_SBB32:
    case HandlerType::SUB_2_SBB16:
    case HandlerType::SUB_2_SBB8:
        CurrentOpcode=SBB;
        break;

    }



    //Analyze Third Handler
    Sub_3Type=GetHandlerType(Sub_3+EnigmaBase);
    switch (Sub_3Type)
    {
    case HandlerType::SUB_3_Single:
        S3_1_Need=true;
        break;
    case HandlerType::SUB_3_NULL:
        S3_1_Need=false;
        if (CurrentOpcode==AND)
            CurrentOpcode=TEST;
        if (CurrentOpcode==SUB)
            CurrentOpcode=CMP;
        break;
    }



    //Analyze Third -> 1 Handler
    if (S3_1_Need)
    {
        Sub_3_1_Type=GetHandlerType(Sub_3_1+EnigmaBase);
        switch (Sub_3_1_Type)
        {
        case HandlerType::SUB_3_1_32:
            Operand1=AnalyzeOperand(Sub_3_1_Type);
            break;
        case HandlerType::SUB_3_1_16:
            Operand1=AnalyzeOperand(Sub_3_1_Type);
            break;
        case HandlerType::SUB_3_1_8:
            Operand1=AnalyzeOperand(Sub_3_1_Type);
            break;
        case HandlerType::SUB_3_1_8High:
            Operand1=AnalyzeOperand(Sub_3_1_Type);
            break;
        }
    }

}

PDISASM CEnigmaAnalyzer::FindInstruction(ULONG_PTR nStart,CPatternDetection::PatternType iType)
{
    for (int i=0; i<(*HandlerInstruction).size(); i++)
    {
        if ((*HandlerInstruction)[i].iType==iType)
            return &(*HandlerInstruction)[i].iDisasm;

    }

    return 0;
}

CEnigmaAnalyzer::ASMOperand CEnigmaAnalyzer::AnalyzeOperand(HandlerType iOperandHandler)
{
    HandlerType iType;
    if (iOperandHandler==Undifined_Handler)
        iType=AnalyzeHandler();
    else
        iType=iOperandHandler;

    ASMOperand iOperand;
    iOperand.bHighReg=false;
    iOperand.OperandSize=0;
    iOperand.RegOrImm=false;
    iOperand.Value=0;
    iOperand.ExtraValue=0;
    iOperand.bMemory=false;
    switch (iType)
    {
    case HandlerType::SUB_3_1_32:
        iOperand.RegOrImm=true;
        iOperand.OperandSize=32;
        iOperand.Value=FindInstruction(0,CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX)->Argument1.Memory.Displacement/4;
        break;
    case HandlerType::SUB_3_1_8:
        iOperand.RegOrImm=true;
        iOperand.OperandSize=8;
        iOperand.Value=FindInstruction(0,CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX)->Argument1.Memory.Displacement/4;
        break;
    case HandlerType::SUB_3_1_8High:
        iOperand.RegOrImm=true;
        iOperand.OperandSize=8;
        iOperand.bHighReg=true;
        iOperand.Value=FindInstruction(0,CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX)->Argument1.Memory.Displacement/4;
        break;
    case HandlerType::SUB_3_1_16:
        iOperand.RegOrImm=true;
        iOperand.OperandSize=16;
        iOperand.Value=FindInstruction(0,CPatternDetection::PatternType::MOV_Mem32_EDXImm_EAX)->Argument1.Memory.Displacement/4;
        break;
    case HandlerType::SUB_1_X_32:
        iOperand.RegOrImm=true;
        iOperand.OperandSize=32;
        iOperand.Value=FindInstruction(0,CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm)->Argument2.Memory.Displacement/4;
        break;
    case HandlerType::SUB_1_X_16:
        iOperand.RegOrImm=true;
        iOperand.OperandSize=16;
        iOperand.Value=FindInstruction(0,CPatternDetection::PatternType::MOV_AX_Mem16_EAXImm)->Argument2.Memory.Displacement/4;
        break;
    case HandlerType::SUB_1_X_8:
        iOperand.RegOrImm=true;
        iOperand.OperandSize=8;
        iOperand.Value=FindInstruction(0,CPatternDetection::PatternType::MOV_AL_Mem8_EAXImm)->Argument2.Memory.Displacement/4;
        break;
    case HandlerType::SUB_1_X_8High:
        iOperand.RegOrImm=true;
        iOperand.OperandSize=8;
        iOperand.bHighReg=true;
        iOperand.Value=FindInstruction(0,CPatternDetection::PatternType::MOV_EAX_Mem32_EAXImm)->Argument2.Memory.Displacement/4;
        break;

    }

    return iOperand;
}

ULONG_PTR  CEnigmaAnalyzer::GetRegIndex(ULONG_PTR iReg)
{
    iReg&=0xFF;

    switch (iReg)
    {
    case EAX:
        return 0;
        break;
    case ECX:
        return 1;
        break;
    case EDX:
        return 2;
        break;
    case EBX:
        return 3;
        break;
    case ESP:
        return 4;
        break;
    case EBP:
        return 5;
        break;
    case ESI:
        return 6;
        break;
    case EDI:
        return 7;
        break;
    };
}
