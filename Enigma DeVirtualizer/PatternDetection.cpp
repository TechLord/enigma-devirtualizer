#include "PatternDetection.h"
#include <Windows.h>

CPatternDetection::CPatternDetection(void)
{
    PatternTable=(ASMPattern*)malloc(sizeof(ASMPattern)*MaxPatternCount);
    ZeroMemory(PatternTable,sizeof(ASMPattern)*MaxPatternCount);


    ULONG_PTR i=0;

    //-----------JMP Imm
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=CONSTANT_TYPE | RELATIVE_;
    PatternTable[i].ASMStruct.Argument2.ArgType=NO_ARGUMENT;
    PatternTable[i].iType=JMP_Imm;
    strcpy_s(PatternTable[i].Mnemonic,"jmp ");
    i++;
    //-----------JMP ULONG_PTR PTR [EBP+X];
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Scale=true;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister = REG5;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister = 0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale = 0;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;
    PatternTable[i].ASMStruct.Argument2.ArgType=NO_ARGUMENT;
    PatternTable[i].iType=JMP_Mem32EBP;
    strcpy_s(PatternTable[i].Mnemonic,"jmp ");
    i++;


    //-----------Call Imm
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=CONSTANT_TYPE | RELATIVE_;
    PatternTable[i].ASMStruct.Argument2.ArgType=NO_ARGUMENT;
    PatternTable[i].iType=CALL_Imm;
    strcpy_s(PatternTable[i].Mnemonic,"call ");
    i++;

    //-----------CALL ULONG_PTR PTR SS:[EBP-0C]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Displacement=true;
    PatternTable[i].Arg1.Scale=true;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Displacement=0xfffffffffffffff4;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;
    PatternTable[i].ASMStruct.Argument2.ArgType=NO_ARGUMENT;
    PatternTable[i].iType=CALL_Mem32_EBP10;
    strcpy_s(PatternTable[i].Mnemonic,"call ");
    i++;

    //-----------CALL ULONG_PTR PTR SS:[EBP-10]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Displacement=true;
    PatternTable[i].Arg1.Scale=true;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Displacement=0xfffffffffffffff0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;
    PatternTable[i].ASMStruct.Argument2.ArgType=NO_ARGUMENT;
    PatternTable[i].iType=CALL_Mem32_EBP10;
    strcpy_s(PatternTable[i].Mnemonic,"call ");
    i++;





    //-----------CMP ULONG_PTR [Imm],Imm
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Scale=true;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;
    PatternTable[i].ASMStruct.Argument2.ArgType=CONSTANT_TYPE | ABSOLUTE_;
    PatternTable[i].iType=CMP_Mem32Imm_Imm;
    strcpy_s(PatternTable[i].Mnemonic,"cmp ");
    i++;

    //-----------MOV R32,ULONG_PTR [Imm]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOV_R32_Mem32Imm;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV ULONG_PTR PTR SS:[EBP-XX],ECX
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Scale=true;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;
    PatternTable[i].ASMStruct.Argument2.ArgType=REGISTER_TYPE | GENERAL_REG | REG1;
    PatternTable[i].iType=MOV_Mem32_EBPImm_ECX;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV ULONG_PTR PTR SS:[EBP-XX],EDX
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Scale=true;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;
    PatternTable[i].ASMStruct.Argument2.ArgType=REGISTER_TYPE | GENERAL_REG | REG2;
    PatternTable[i].iType=MOV_Mem32_EBPImm_EDX;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV ULONG_PTR PTR SS:[EBP-XX],EAX
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Scale=true;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;
    PatternTable[i].ASMStruct.Argument2.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].iType=MOV_Mem32_EBPImm_EAX;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV WORD PTR [EBP-XX],AX
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Scale=true;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=16;
    PatternTable[i].ASMStruct.Argument2.ArgSize=16;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;
    PatternTable[i].ASMStruct.Argument2.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].iType=MOV_Mem16_EBPImm_AX;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV AL,BYTE PTR DS:[EAX+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG0;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=8;
    PatternTable[i].ASMStruct.Argument2.ArgSize=8;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOV_AL_Mem8_EAXImm;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV AL,BYTE PTR DS:[EBX+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG3;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=8;
    PatternTable[i].ASMStruct.Argument2.ArgSize=8;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOV_AL_Mem8_EBXImm;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV DL,BYTE PTR SS:[EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=8;
    PatternTable[i].ASMStruct.Argument2.ArgSize=8;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG2;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOV_DL_Mem8_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV AL,BYTE PTR SS:[EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=8;
    PatternTable[i].ASMStruct.Argument2.ArgSize=8;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOV_AL_Mem8_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV EDX,ULONG_PTR PTR SS:[EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG2;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOV_EDX_Mem32_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;

    //-----------MOVZX EAX,WORD PTR [EAX+Imm]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG0;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=16;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOVZX_EAX_Mem16_EAXImm;
    strcpy_s(PatternTable[i].Mnemonic,"movzx ");
    i++;

    //-----------MOVZX EDX,WORD PTR [EBP-8]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=16;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG2;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOVZX_EDX_Mem16_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"movzx ");
    i++;

    //-----------MOVZX EDX,BYTE PTR DS:[EDX+Imm]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG2;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=8;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG2;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOVZX_EDX_Mem8_EDXImm;
    strcpy_s(PatternTable[i].Mnemonic,"movzx ");
    i++;
    //-----------MOVZX EAX,WORD PTR DS:[EBX+Imm]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG3;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=16;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOVZX_EAX_Mem16_EBXImm;
    strcpy_s(PatternTable[i].Mnemonic,"movzx ");
    i++;
    //-----------MOVZX EAX,BYTE PTR [EAX+Imm]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG0;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=8;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOVZX_EAX_Mem8_EAXImm;
    strcpy_s(PatternTable[i].Mnemonic,"movzx ");
    i++;
    //-----------MOV AX,WORD PTR [EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=16;
    PatternTable[i].ASMStruct.Argument2.ArgSize=16;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOV_AX_Mem16_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV DX,WORD PTR [EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=16;
    PatternTable[i].ASMStruct.Argument2.ArgSize=16;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG2;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOV_DX_Mem16_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;

    //-----------MOV EAX,ULONG_PTR PTR SS:[EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOV_EAX_Mem32_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV EAX,ULONG_PTR PTR SS:[EAX+Imm]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG0;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOV_EAX_Mem32_EAXImm;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;

    //-----------MOV EAX,ULONG_PTR PTR SS:[EBX+Imm]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG3;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOV_EAX_Mem32_EBXImm;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV ECX,ULONG_PTR PTR [EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG1;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOV_ECX_Mem32_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    /*//-----------MOV ECX,ULONG_PTR PTR [ECX+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG1;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG1;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOV_ECX_Mem32_ECXImm;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;*/
    //-----------MOV EDX,ULONG_PTR PTR [EDX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].Arg2.Displacement=true;
    PatternTable[i].ASMStruct.Argument2.Memory.Displacement=0;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG2;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG2;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOV_EDX_Mem32_EDX;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV ULONG_PTR PTR DS:[EDX+Imm],EAX
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Scale=true;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister=REG2;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;
    PatternTable[i].ASMStruct.Argument2.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].iType=MOV_Mem32_EDXImm_EAX;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV ULONG_PTR PTR DS:[EAX],EDX
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Scale=true;
    PatternTable[i].Arg1.Displacement=true;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister=REG0;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Displacement=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;
    PatternTable[i].ASMStruct.Argument2.ArgType=REGISTER_TYPE | GENERAL_REG | REG2;
    PatternTable[i].iType=MOV_Mem32_EAX_EDX;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV WORD PTR DS:[EAX+XX],DX
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Scale=true;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister=REG0;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=16;
    PatternTable[i].ASMStruct.Argument2.ArgSize=16;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;
    PatternTable[i].ASMStruct.Argument2.ArgType=REGISTER_TYPE | GENERAL_REG | REG2;
    PatternTable[i].iType=MOV_Mem16_EAXImm_DX;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV WORD PTR DS:[EDX+XX],AX
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Scale=true;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister=REG2;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=16;
    PatternTable[i].ASMStruct.Argument2.ArgSize=16;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;
    PatternTable[i].ASMStruct.Argument2.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].iType=MOV_Mem16_EDXImm_AX;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV BYTE PTR SS:[EBP+XX],AL
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Scale=true;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=8;
    PatternTable[i].ASMStruct.Argument2.ArgSize=8;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;
    PatternTable[i].ASMStruct.Argument2.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].iType=MOV_Mem8_EBPImm_AL;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV BYTE PTR SS:[EDX+XX],AL
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Scale=true;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister=REG2;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=8;
    PatternTable[i].ASMStruct.Argument2.ArgSize=8;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;
    PatternTable[i].ASMStruct.Argument2.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].iType=MOV_Mem8_EDXImm_AL;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV BYTE PTR SS:[EAX+XX],DL
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Scale=true;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister=REG0;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=8;
    PatternTable[i].ASMStruct.Argument2.ArgSize=8;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;
    PatternTable[i].ASMStruct.Argument2.ArgType=REGISTER_TYPE | GENERAL_REG | REG2;
    PatternTable[i].iType=MOV_Mem8_EAXImm_DL;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV AX,WORD PTR [EAX+Imm]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG0;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=16;
    PatternTable[i].ASMStruct.Argument2.ArgSize=16;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOV_AX_Mem16_EAXImm;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;

    //-----------ADD AL,BYTE PTR SS:[EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=8;
    PatternTable[i].ASMStruct.Argument2.ArgSize=8;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=ADD_AL_Mem8_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"add ");
    i++;
    //-----------ADD AX,WORD PTR [EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=16;
    PatternTable[i].ASMStruct.Argument2.ArgSize=16;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=ADD_AX_Mem16_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"add ");
    i++;

    //-----------ADD EAX,EDX
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=REGISTER_TYPE | GENERAL_REG | REG2;
    PatternTable[i].iType=ADD_EAX_EDX;
    strcpy_s(PatternTable[i].Mnemonic,"add ");
    i++;

    //-----------ADD EBX,EAX
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG3;
    PatternTable[i].ASMStruct.Argument2.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].iType=ADD_EBX_EAX;
    strcpy_s(PatternTable[i].Mnemonic,"add ");
    i++;

    //-----------ADD EAX,ULONG_PTR PTR DS:[EDX+Imm]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG2;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=ADD_EAX_Mem32_EDXImm;
    strcpy_s(PatternTable[i].Mnemonic,"add ");
    i++;

    //-----------ADD EBX,ULONG_PTR PTR DS:[EAX+Imm]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG0;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG3;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=ADD_EBX_Mem32_EAXImm;
    strcpy_s(PatternTable[i].Mnemonic,"add ");
    i++;
    //-----------ADD EAX,ULONG_PTR PTR SS:[EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=ADD_EAX_Mem32_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"add ");
    i++;

    //-----------SUB EAX,ULONG_PTR PTR [EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=SUB_EAX_Mem32_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"sub ");
    i++;
    //-----------SUB AX,WORD PTR [EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=16;
    PatternTable[i].ASMStruct.Argument2.ArgSize=16;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=SUB_AX_Mem16_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"sub ");
    i++;
    //-----------SUB AL,BYTE PTR SS:[EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=8;
    PatternTable[i].ASMStruct.Argument2.ArgSize=8;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=SUB_AL_Mem8_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"sub ");
    i++;


    //-----------XOR EAX,ULONG_PTR PTR [EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=XOR_EAX_Mem32_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"xor ");
    i++;
    //-----------XOR AX,WORD PTR [EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=16;
    PatternTable[i].ASMStruct.Argument2.ArgSize=16;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=XOR_AX_Mem16_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"xor ");
    i++;
    //-----------XOR AL,BYTE PTR SS:[EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=8;
    PatternTable[i].ASMStruct.Argument2.ArgSize=8;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=XOR_AL_Mem8_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"xor ");
    i++;


    //-----------OR EAX,ULONG_PTR PTR [EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=OR_EAX_Mem32_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"or ");
    i++;
    //-----------OR AX,WORD PTR [EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=16;
    PatternTable[i].ASMStruct.Argument2.ArgSize=16;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=OR_AX_Mem16_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"or ");
    i++;
    //-----------OR AL,BYTE PTR SS:[EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=8;
    PatternTable[i].ASMStruct.Argument2.ArgSize=8;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=OR_AL_Mem8_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"or ");
    i++;

    //-----------AND EAX,EDI
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=REGISTER_TYPE | GENERAL_REG | REG7;
    PatternTable[i].iType=AND_EAX_EDI;
    strcpy_s(PatternTable[i].Mnemonic,"and ");
    i++;
    //-----------AND EAX,ULONG_PTR PTR [EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=AND_EAX_Mem32_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"and ");
    i++;

    //-----------AND EAX,Imm
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=CONSTANT_TYPE | ABSOLUTE_;
    PatternTable[i].iType=AND_EAX_Imm;
    strcpy_s(PatternTable[i].Mnemonic,"and ");
    i++;

    //-----------AND AX,WORD PTR [EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=16;
    PatternTable[i].ASMStruct.Argument2.ArgSize=16;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=AND_AX_Mem16_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"and ");
    i++;
    //-----------AND AL,BYTE PTR [EBP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=8;
    PatternTable[i].ASMStruct.Argument2.ArgSize=8;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=AND_AL_Mem8_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"and ");
    i++;
    //-----------SHL EDX,Imm
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG2;
    PatternTable[i].ASMStruct.Argument2.ArgType=CONSTANT_TYPE | ABSOLUTE_;
    PatternTable[i].iType=SHL_EDX_Imm;
    strcpy_s(PatternTable[i].Mnemonic,"shl ");
    i++;
    //-----------SHR EAX,Imm
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=CONSTANT_TYPE | ABSOLUTE_;
    PatternTable[i].iType=SHR_EAX_Imm;
    strcpy_s(PatternTable[i].Mnemonic,"shr ");
    i++;

    //-----------CMP R32,Imm
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG;
    PatternTable[i].ASMStruct.Argument2.ArgType=CONSTANT_TYPE | ABSOLUTE_;
    PatternTable[i].iType=CMP_R32_Imm;
    strcpy_s(PatternTable[i].Mnemonic,"cmp ");
    i++;


    //-----------INC EAX
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=0;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=NO_ARGUMENT;
    PatternTable[i].iType=INC_EAX;
    strcpy_s(PatternTable[i].Mnemonic,"inc ");
    i++;

    //INC ULONG_PTR PTR SS:[EBP-XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Scale=true;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;;
    PatternTable[i].ASMStruct.Argument2.ArgType=NO_ARGUMENT;
    PatternTable[i].iType=INC_Mem32_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"inc ");
    i++;

    //INC WORD PTR SS:[EBP-XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Scale=true;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=16;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;;
    PatternTable[i].ASMStruct.Argument2.ArgType=NO_ARGUMENT;
    PatternTable[i].iType=INC_Mem16_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"inc ");
    i++;

    //INC BYTE PTR SS:[EBP-XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Scale=true;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister=REG5;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=8;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;;
    PatternTable[i].ASMStruct.Argument2.ArgType=NO_ARGUMENT;
    PatternTable[i].iType=INC_Mem8_EBPImm;
    strcpy_s(PatternTable[i].Mnemonic,"inc ");
    i++;


    //-----------JE Imm
    PatternTable[i].ASMStruct.Argument1.ArgType=CONSTANT_TYPE | RELATIVE_;
    PatternTable[i].ASMStruct.Argument2.ArgType=NO_ARGUMENT;
    PatternTable[i].iType=JE_Imm;
    strcpy_s(PatternTable[i].Mnemonic,"je ");
    i++;


    //-----------JNE Imm
    PatternTable[i].ASMStruct.Argument1.ArgType=CONSTANT_TYPE | RELATIVE_;
    PatternTable[i].ASMStruct.Argument2.ArgType=NO_ARGUMENT;
    PatternTable[i].iType=JNE_Imm;
    strcpy_s(PatternTable[i].Mnemonic,"jne ");
    i++;



    //-----------RET
    PatternTable[i].ASMStruct.Argument1.ArgType=NO_ARGUMENT;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=RET;
    strcpy_s(PatternTable[i].Mnemonic,"ret ");
    i++;

    //-----------RET X
    PatternTable[i].ASMStruct.Argument1.ArgType=CONSTANT_TYPE | ABSOLUTE_;
    PatternTable[i].ASMStruct.Argument2.ArgType=NO_ARGUMENT;
    PatternTable[i].iType=RET_X;
    strcpy_s(PatternTable[i].Mnemonic,"retn ");
    i++;



    //-----------MOV R32,ULONG_PTR PTR [ESP+XX]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Displacement=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG4;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Displacement=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=MOV_R32_Mem32_ESP;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;
    //-----------MOV ULONG_PTR PTR DS:[ESP],R32
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Displacement=true;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister=REG4;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Displacement=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;
    PatternTable[i].ASMStruct.Argument2.ArgType=REGISTER_TYPE | GENERAL_REG;
    PatternTable[i].iType=MOV_Mem32_ESP_R32;
    strcpy_s(PatternTable[i].Mnemonic,"mov ");
    i++;

    //-----------LEA ESP,[ESP+4]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Displacement=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG4;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Displacement=4;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG4;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=LEA_ESP_Mem32_ESP4;
    strcpy_s(PatternTable[i].Mnemonic,"lea ");
    i++;
    //-----------LEA ESP,[ESP-4]
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Displacement=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG4;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Displacement=-4;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG4;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=LEA_ESP_Mem32_ESP_4;
    strcpy_s(PatternTable[i].Mnemonic,"lea ");
    i++;

    //-----------PUSH R32
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Scale=true;
    PatternTable[i].Arg1.Displacement=true;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister=REG4;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Displacement=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;
    PatternTable[i].ASMStruct.Argument2.ArgType=REGISTER_TYPE | GENERAL_REG;
    PatternTable[i].iType=PUSH_R32;
    strcpy_s(PatternTable[i].Mnemonic,"push ");
    i++;

    //-----------POP R32
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].Arg2.Displacement=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG4;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Displacement=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=POP_R32;
    strcpy_s(PatternTable[i].Mnemonic,"pop ");
    i++;

    //-----------PUSHFD
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg1.BaseRegister=true;
    PatternTable[i].Arg1.IndexRegister=true;
    PatternTable[i].Arg1.Scale=true;
    PatternTable[i].Arg1.Displacement=true;
    PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister=REG4;
    PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.Memory.Displacement=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=MEMORY_TYPE;
    PatternTable[i].ASMStruct.Argument2.ArgType=REGISTER_TYPE | REG0;
    PatternTable[i].iType=PUSHFD;
    strcpy_s(PatternTable[i].Mnemonic,"pushfd ");
    i++;

    //-----------POP R32
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].Arg2.BaseRegister=true;
    PatternTable[i].Arg2.IndexRegister=true;
    PatternTable[i].Arg2.Scale=true;
    PatternTable[i].Arg2.Displacement=true;
    PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister=REG4;
    PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Displacement=0;
    PatternTable[i].ASMStruct.Argument2.Memory.Scale=0;
    PatternTable[i].ASMStruct.Argument1.ArgSize=32;
    PatternTable[i].ASMStruct.Argument2.ArgSize=32;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | SPECIAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=MEMORY_TYPE;
    PatternTable[i].iType=POPFD;
    strcpy_s(PatternTable[i].Mnemonic,"popfd ");
    i++;


    //-----------TEST AL,AL
    PatternTable[i].Arg1.ArgSize=true;
    PatternTable[i].Arg2.ArgSize=true;
    PatternTable[i].ASMStruct.Argument1.ArgSize=8;
    PatternTable[i].ASMStruct.Argument2.ArgSize=8;
    PatternTable[i].ASMStruct.Argument1.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].ASMStruct.Argument2.ArgType=REGISTER_TYPE | GENERAL_REG | REG0;
    PatternTable[i].iType=TEST_AL_AL;
    strcpy_s(PatternTable[i].Mnemonic,"test ");
    i++;


    PatternCount=i;

}


CPatternDetection::~CPatternDetection(void)
{
    delete[] PatternTable;
}
CPatternDetection::PatternType CPatternDetection::ValidatePattern(DISASM Target)
{

    for (unsigned int i=0; i<PatternCount; i++)
    {
        LPSTR strTarget=CharLowerA(Target.Instruction.Mnemonic);
        LPSTR strPattern=CharLowerA(PatternTable[i].Mnemonic);

        if (strcmp(strTarget,strPattern)!=0)
            continue;

        //---------Type Info
        if ((PatternTable[i].ASMStruct.Argument1.ArgType & Target.Argument1.ArgType)!=(PatternTable[i].ASMStruct.Argument1.ArgType))
            continue;

        if ((PatternTable[i].ASMStruct.Argument2.ArgType & Target.Argument2.ArgType)!=(PatternTable[i].ASMStruct.Argument2.ArgType))
            continue;


        if (PatternTable[i].RepPrefix==1 && (Target.Prefix.RepnePrefix==InUsePrefix || Target.Prefix.RepPrefix==InUsePrefix))
            continue;
        if (PatternTable[i].RepPrefix && ((PatternTable[i].RepPrefix==2 && Target.Prefix.RepPrefix!=InUsePrefix) || (PatternTable[i].RepPrefix==3 && Target.Prefix.RepnePrefix!=InUsePrefix)))
            continue;


        //----------Arg1 Check
        if (PatternTable[i].Arg1.ArgSize)
            if (PatternTable[i].ASMStruct.Argument1.ArgSize!=Target.Argument1.ArgSize)
                continue;

        if (PatternTable[i].Arg1.BaseRegister)
            if (PatternTable[i].ASMStruct.Argument1.Memory.BaseRegister!=Target.Argument1.Memory.BaseRegister)
                continue;

        if (PatternTable[i].Arg1.Displacement)
            if (PatternTable[i].ASMStruct.Argument1.Memory.Displacement!=Target.Argument1.Memory.Displacement)
                continue;

        if (PatternTable[i].Arg1.IndexRegister)
            if (PatternTable[i].ASMStruct.Argument1.Memory.IndexRegister!=Target.Argument1.Memory.IndexRegister)
                continue;

        if (PatternTable[i].Arg1.Scale)
            if (PatternTable[i].ASMStruct.Argument1.Memory.Scale!=Target.Argument1.Memory.Scale)
                continue;

        //----------Arg2 Check
        if (PatternTable[i].Arg2.ArgSize)
            if (PatternTable[i].ASMStruct.Argument2.ArgSize!=Target.Argument2.ArgSize)
                continue;

        if (PatternTable[i].Arg2.BaseRegister)
            if (PatternTable[i].ASMStruct.Argument2.Memory.BaseRegister!=Target.Argument2.Memory.BaseRegister)
                continue;

        if (PatternTable[i].Arg2.Displacement)
            if (PatternTable[i].ASMStruct.Argument2.Memory.Displacement!=Target.Argument2.Memory.Displacement)
                continue;

        if (PatternTable[i].Arg2.IndexRegister)
            if (PatternTable[i].ASMStruct.Argument2.Memory.IndexRegister!=Target.Argument2.Memory.IndexRegister)
                continue;

        if (PatternTable[i].Arg2.Scale)
            if (PatternTable[i].ASMStruct.Argument2.Memory.Scale!=Target.Argument2.Memory.Scale)
                continue;

        if (PatternTable[i].SegFS)
            if (Target.Prefix.FSPrefix!=InUsePrefix)
                continue;
        if (!PatternTable[i].SegFS)
            if (Target.Prefix.FSPrefix==InUsePrefix)
                continue;

        return PatternTable[i].iType;

    }



    return NotFound;
}


char* CPatternDetection::GetHandlerString(PatternType iType)
{

    switch (iType)
    {
    case PatternType::SUB_EAX_Mem32_EBPImm:
        return "SUB_EAX_Mem32_EBPImm";
        break;
    case PatternType::SUB_AX_Mem16_EBPImm:
        return "SUB_AX_Mem16_EBPImm";
        break;
    case PatternType::SUB_AL_Mem8_EBPImm:
        return "SUB_AL_Mem8_EBPImm";
        break;
    case PatternType::ADD_AL_Mem8_EBPImm:
        return "ADD_AL_Mem8_EBPImm";
        break;
    case PatternType::ADD_AX_Mem16_EBPImm:
        return "ADD_AX_Mem16_EBPImm";
        break;
    case PatternType::ADD_EAX_EDX:
        return "ADD_EAX_EDX";
        break;
    case PatternType::AND_EAX_EDI:
        return "AND_EAX_EDI";
        break;
    case PatternType::ADD_EAX_Mem32_EBPImm:
        return "ADD_EAX_Mem32_EBPImm";
        break;
    case PatternType::ADD_EAX_Mem32_EDXImm:
        return "ADD_EAX_Mem32_EDXImm";
        break;
    case PatternType::ADD_EBX_Mem32_EAXImm:
        return "ADD_EBX_Mem32_EAXImm";
        break;
    case PatternType::ADD_EBX_EAX:
        return "ADD_EBX_EAX";
        break;
    case PatternType::AND_EAX_Imm:
        return "AND_EAX_Imm";
        break;
    case PatternType::AND_EAX_Mem32_EBPImm:
        return "AND_EAX_Mem32_EBPImm";
        break;
    case PatternType::AND_AX_Mem16_EBPImm:
        return "AND_AX_Mem16_EBPImm";
        break;
    case PatternType::AND_AL_Mem8_EBPImm:
        return "AND_AL_Mem8_EBPImm";
        break;
    case PatternType::CALL_Imm:
        return "CALL_Imm";
        break;
    case PatternType::CALL_Mem32_EBP0C:
        return "CALL_Mem32_EBP0C";
        break;
    case PatternType::CALL_Mem32_EBP10:
        return "CALL_Mem32_EBP10";
        break;
    case PatternType::CMP_Mem32Imm_Imm:
        return "CMP_Mem32Imm_Imm";
        break;
    case PatternType::CMP_R32_Imm:
        return "CMP_R32_Imm";
        break;
    case PatternType::INC_EAX:
        return "INC_EAX";
        break;
    case PatternType::INC_Mem32_EBPImm:
        return "INC_Mem32_EBPImm";
        break;
    case PatternType::INC_Mem16_EBPImm:
        return "INC_Mem16_EBPImm";
        break;
    case PatternType::INC_Mem8_EBPImm:
        return "INC_Mem8_EBPImm";
        break;
    case PatternType::JMP_Imm:
        return "JMP_Imm";
        break;
    case PatternType::JMP_Mem32EBP:
        return "JMP_Mem32EBP";
        break;
    case PatternType::MOV_AX_Mem16_EAXImm:
        return "MOV_AX_Mem16_EAXImm";
        break;
    case PatternType::MOV_AL_Mem8_EAXImm:
        return "MOV_AL_Mem8_EAXImm";
        break;
    case PatternType::MOV_AL_Mem8_EBXImm:
        return "MOV_AL_Mem8_EBXImm";
        break;
    case PatternType::MOV_Mem8_EBPImm_AL:
        return "MOV_Mem8_EBPImm_AL";
        break;
    case PatternType::MOV_EAX_Mem32_EAXImm:
        return "MOV_EAX_Mem32_EAXImm";
        break;
    case PatternType::MOV_EAX_Mem32_EBPImm:
        return "MOV_EAX_Mem32_EBPImm";
        break;
    case PatternType::MOV_EAX_Mem32_EBXImm:
        return "MOV_EAX_Mem32_EBXImm";
        break;
    case PatternType::MOV_ECX_Mem32_EBPImm:
        return "MOV_ECX_Mem32_EBPImm";
        break;
    case PatternType::MOV_ECX_Mem32_ECXImm:
        return "MOV_ECX_Mem32_ECXImm";
        break;
    case PatternType::MOV_EDX_Mem32_EDX:
        return "MOV_EDX_Mem32_EDX";
        break;
    case PatternType::MOV_EDX_Mem32_EBPImm:
        return "MOV_EDX_Mem32_EBPImm";
        break;
    case PatternType::MOV_Mem32_EBPImm_EAX:
        return "MOV_Mem32_EBPImm_EAX";
        break;
    case PatternType::MOV_Mem32_EBPImm_ECX:
        return "MOV_Mem32_EBPImm_ECX";
        break;
    case PatternType::MOV_Mem32_EBPImm_EDX:
        return "MOV_Mem32_EBPImm_EDX";
        break;
    case PatternType::MOV_Mem32_EAX_EDX:
        return "MOV_Mem32_EAX_EDX";
        break;
    case PatternType::MOV_Mem32_EDXImm_EAX:
        return "MOV_Mem32_EDXImm_EAX";
        break;
    case PatternType::MOV_Mem16_EAXImm_DX:
        return "MOV_Mem16_EAXImm_DX";
        break;

    case PatternType::MOV_Mem16_EDXImm_AX:
        return "MOV_Mem16_EDXImm_AX";
        break;
    case PatternType::MOV_Mem16_EBPImm_AX:
        return "MOV_Mem16_EBPImm_AX";
        break;
    case PatternType::MOV_Mem8_EDXImm_AL:
        return "MOV_Mem8_EDXImm_AL";
        break;
    case PatternType::MOV_Mem8_EAXImm_DL:
        return "MOV_Mem8_EAXImm_DL";
        break;
    case PatternType::MOV_AL_Mem8_EBPImm:
        return "MOV_AL_Mem8_EBPImm";
        break;
    case PatternType::MOV_AX_Mem16_EBPImm:
        return "MOV_AX_Mem16_EBPImm";
        break;
    case PatternType::MOV_DX_Mem16_EBPImm:
        return "MOV_DX_Mem16_EBPImm";
        break;
    case PatternType::MOV_DL_Mem8_EBPImm:
        return "MOV_DL_Mem8_EBPImm";
        break;
    case PatternType::MOV_R32_Mem32Imm:
        return "MOV_R32_Mem32Imm";
        break;
    case PatternType::MOVZX_EAX_Mem16_EAXImm:
        return "MOVZX_EAX_Mem16_EAXImm";
        break;
    case PatternType::MOVZX_EDX_Mem16_EBPImm:
        return "MOVZX_EDX_Mem16_EBPImm";
        break;
    case PatternType::MOVZX_EAX_Mem8_EAXImm:
        return "MOVZX_EAX_Mem8_EAXImm";
        break;
    case PatternType::MOVZX_EAX_Mem16_EBXImm:
        return "MOVZX_EAX_Mem16_EBXImm";
        break;
    case PatternType::MOVZX_EDX_Mem8_EDXImm:
        return "MOVZX_EDX_Mem8_EDXImm";
        break;
    case PatternType::SHL_EDX_Imm:
        return "SHL_EDX_Imm";
        break;
    case PatternType::SHR_EAX_Imm:
        return "SHR_EAX_Imm";
        break;
    case PatternType::RET:
        return "RET";
        break;
    case PatternType::RET_X:
        return "RET_X";
        break;
    case PatternType::OR_EAX_Mem32_EBPImm:
        return "OR_EAX_Mem32_EBPImm";
        break;
    case PatternType::OR_AX_Mem16_EBPImm:
        return "OR_AX_Mem16_EBPImm";
        break;
    case PatternType::OR_AL_Mem8_EBPImm:
        return "OR_AL_Mem8_EBPImm";
        break;
    case PatternType::XOR_EAX_Mem32_EBPImm:
        return "XOR_EAX_Mem32_EBPImm";
        break;
    case PatternType::XOR_AX_Mem16_EBPImm:
        return "XOR_AX_Mem16_EBPImm";
        break;
    case PatternType::XOR_AL_Mem8_EBPImm:
        return "XOR_AL_Mem8_EBPImm";
        break;

    case PatternType::NotFound:
        return "NotFound";
        break;



    };



}
