#pragma once
#include "BeaEngine\BeaEngine.h"
#include <Windows.h>
class CPatternDetection
{
private:
#define MaxPatternCount 256

public:
    enum PatternType {NotFound,

                      JMP_Imm,JMP_Mem32EBP,


                      CALL_Imm,CALL_Mem32_EBP10,CALL_Mem32_EBP0C,


                      CMP_Mem32Imm_Imm,CMP_R32_Imm,


                      MOV_R32_Mem32Imm,MOV_Mem32_EBPImm_ECX,MOV_Mem32_EBPImm_EDX,MOV_Mem32_EBPImm_EAX,MOV_AL_Mem8_EAXImm,MOV_EDX_Mem32_EBPImm,MOV_EAX_Mem32_EBPImm,MOV_Mem32_EDXImm_EAX,MOV_Mem32_EAX_EDX,MOV_EAX_Mem32_EAXImm,MOV_EDX_Mem32_EDX,MOV_ECX_Mem32_EBPImm,MOV_Mem8_EAXImm_DL,
                      MOV_AX_Mem16_EAXImm,MOV_Mem16_EBPImm_AX,MOV_DX_Mem16_EBPImm,MOV_Mem16_EAXImm_DX,MOVZX_EDX_Mem16_EBPImm,MOVZX_EAX_Mem16_EBXImm,MOV_AX_Mem16_EBPImm,MOV_Mem16_EDXImm_AX,MOV_Mem8_EBPImm_AL,MOV_DL_Mem8_EBPImm,MOV_AL_Mem8_EBPImm,MOV_Mem8_EDXImm_AL,MOVZX_EAX_Mem8_EAXImm,MOV_EAX_Mem32_EBXImm,
                      MOVZX_EDX_Mem8_EDXImm,MOV_AL_Mem8_EBXImm,MOV_ECX_Mem32_ECXImm,MOVZX_EAX_Mem16_EAXImm,



                      ADD_EAX_Mem32_EDXImm,ADD_EAX_Mem32_EBPImm,ADD_EAX_EDX,ADD_AX_Mem16_EBPImm,ADD_AL_Mem8_EBPImm,
                      ADD_EBX_Mem32_EAXImm,ADD_EBX_EAX,



                      AND_EAX_Imm,AND_EAX_Mem32_EBPImm,AND_AX_Mem16_EBPImm,AND_AL_Mem8_EBPImm,AND_EAX_EDI,



                      SUB_AL_Mem8_EBPImm,SUB_AX_Mem16_EBPImm,SUB_EAX_Mem32_EBPImm,


                      OR_AL_Mem8_EBPImm,OR_AX_Mem16_EBPImm,OR_EAX_Mem32_EBPImm,



                      XOR_AL_Mem8_EBPImm,XOR_AX_Mem16_EBPImm,XOR_EAX_Mem32_EBPImm,


                      SHR_EAX_Imm,SHL_EDX_Imm,


                      INC_EAX,INC_Mem32_EBPImm,INC_Mem16_EBPImm,INC_Mem8_EBPImm,



                      JE_Imm,JNE_Imm,


                      MOV_R32_Mem32_ESP,MOV_Mem32_ESP_R32,
                      LEA_ESP_Mem32_ESP4,LEA_ESP_Mem32_ESP_4,
                      PUSH_R32,POP_R32,PUSHFD,POPFD,


                      TEST_AL_AL,

                      RET,RET_X

                     };
    struct ArgumantCheck
    {
        bool ArgSize;
        bool BaseRegister;
        bool IndexRegister;
        bool Scale;
        bool Displacement;

    };
    struct ASMPattern
    {
        char Mnemonic[10];
        PatternType iType;
        DISASM ASMStruct;
        ArgumantCheck Arg1;
        ArgumantCheck Arg2;
        ULONG_PTR RepPrefix; // 0:Not Check 1:Not Prefix 2:Repe 3:Repne
        bool SegFS;
    };


    ASMPattern *PatternTable;
    ULONG_PTR PatternCount;

    CPatternDetection(void);
    ~CPatternDetection(void);

    PatternType ValidatePattern(DISASM Target);
    static char* GetHandlerString(PatternType iType);

};

